import React , {Component} from 'react';
import FontIcon from 'material-ui/FontIcon';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import Avatar from 'material-ui/Avatar';
import Checkbox from 'material-ui/Checkbox'
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import DatePicker from 'material-ui/DatePicker';
import './RegAdmin.css';
import {blue100, blue500, blue700} from 'material-ui/styles/colors';
import Login from './Login';
import axios from 'axios';
import Fin from 'mui-icons/evilicons/check';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import Divider from 'material-ui/Divider';
import Connecton from './Connection'

const server = Connecton.server;
const serverphp = Connecton.serverphp;
const protocol = Connecton.protocol;


const styles = {
  underlineStyle: {
    borderColor: '#9e9e9e',
  },
  labelStyle: {
    color: '#9e9e9e',
  },
  floatingLabelFocusStyle: {
    color: blue500,
  },
};

const customContentStyle = {
  width: '95%',
  maxWidth: '700px',
}

export default class RegAdmin extends Component {

constructor(props) {
  super(props);
  this.state = {
    firstname: null,
    lastname: null,
    bday: null,
    username: null,
    password: null,
    cfrmpassword: null,
    session: false,
    code: null,
    cpnyname: null,
    cpnyadrs: null,
    cpnyid: null,
    cpnycontact: null,
    open: false,
  };

  //this.handleChange = this.handleChange.bind(this);
  this.handleSubmit = this.handleSubmit.bind(this);
}

componentDidMount() {
  this.handleCheckSession();
  window.scrollTo(0, 0);
}

handleFname = (event) => {
    this.setState({
      firstname: event.target.value,
    });
};

handleLname = (event) => {
    this.setState({
      lastname: event.target.value,
    });
};
handleChangeDate = (event, date) => {
    this.setState({
      bday: date,
    });
};
handleUser= (event) => {
    this.setState({
      username: event.target.value,
    });
};
handlePass= (event) => {
    this.setState({
      password: event.target.value,
    });
};
handlePassCfrm= (event) => {
    this.setState({
      cfrmpassword: event.target.value,
    });
};
handleCpnyName= (event) => {
  this.setState({
    cpnyname: event.target.value,
  });
};
handleCpnyAdrs= (event) => {
  this.setState({
    cpnyadrs: event.target.value,
  });
};
handleCpnyID= (event) => {
  this.setState({
    cpnyid: event.target.value,
  });
};
handleCpnyContact= (event) => {
  this.setState({
    cpnycontact: event.target.value,
  });
};

handleSubmit(event) {
  axios.get(protocol+'://'+serverphp+'/php/insertAdmin.php?x='+JSON.stringify(this.state))
    .then(res => {
      if(res.data == '1'){
         localStorage.setItem('user',this.state.username);
         localStorage.setItem('pass',this.state.password);
         window.location.replace(protocol+"://"+server+"/Admin");
      }
      else if(res.data == '0'){
        window.location.replace(protocol+"://"+server+"/AccessCode");
      }
      else{
        alert(res.data);
      }
  });
}

handleCheckSession(){
  var cde = localStorage.getItem('code');
  if(this.state.code == null){
  this.setState({code: cde});
  }
  axios.get(protocol+'://'+serverphp+'/php/checkSessionRegAdmin.php?code='+cde)
    .then(res => {
        if(res.data == '0'){
          window.location.replace(protocol+"://"+server+"/Login");
        }else{
          this.setState({session: true});
        }
  });
}

_handleKeyPress = (e) => {
  if (e.key === 'Enter') {
    this.setState({open: true})
  }
}

handleOpen = () => {
  this.setState({open: true});
};

handleClose = () => {
  this.setState({open: false});
};

render(){
const actions = [
    <FlatButton
        label="Cancel"
        primary={true}
        onClick={this.handleClose}
    />,
    <FlatButton
        label="Accept"
        primary={true}
        keyboardFocused={true}
        onClick={this.handleSubmit}
    />,
];

if(!this.state.session){
  return(<div></div>);
}
return(
    <div>
      <div className="body">
        <div className="back">
          
        </div>
      </div>
      <div className="contmain">
        <div className="contchild" >
          <Avatar src={require('./images/Logo.png')} size={100} className="logos" backgroundColor="white"/>
          <div className="childcontents">
            <br/>
            <br/>
            <br/>
            <div className="blockdivs">
               <h2 className="bluetxt" style={{color: blue500}}>ADMIN SIGN UP</h2>
               <TextField value={this.state.firstname} onChange={this.handleFname} floatingLabelText="First name" fullWidth="true" underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}/>
            </div>
            <div className="blockdivs right fixdiv">
               <h2 className="bluetxt fixing">THE COCIRCLE</h2>
               <TextField value={this.state.lastname} onChange={this.handleLname} floatingLabelText="Last name" fullWidth="true" underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}/>
            </div>
            <div className="blockdivs divsmargin">
               <TextField type="date" floatingLabelFixed="true" floatingLabelText="Birthday" fullWidth="true" value={this.state.bday} onChange={this.handleChangeDate} underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}/>
            </div>
            <div className="blockdivs divsmargin right">
               <TextField type="text" value={this.state.cfrmpassword} onChange={this.handlePassCfrm} floatingLabelText="Mobile" fullWidth="true" underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}/>
            </div>    
            <div className="blockdivs right divsmargin">
                <TextField value={this.state.cpnyadrs} onChange={this.handleCpnyAdrs} floatingLabelText="Company Address" fullWidth="true" underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}/>
            </div>       
            <div className="blockdivs divsmargin">
               <TextField value={this.state.cpnyname} onChange={this.handleCpnyName} floatingLabelText="Company Name" fullWidth="true" underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}/>
            </div>
            <div className="blockdivs right divsmargin">
                <TextField value={this.state.cpnycontact} onChange={this.handleCpnyContact} floatingLabelText="Company Contact" fullWidth="true" underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}/>
            </div>
            <div className="blockdivs divsmargin">
               <TextField  value={this.state.cpnyid} onChange={this.handleCpnyID} floatingLabelText="Company ID" fullWidth="true" underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}/>
            </div>
            <div className="blockdivs right divsmargin">
               <TextField type="password" value={this.state.password} onChange={this.handlePass} floatingLabelText="Password" fullWidth="true" underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}/>
            </div>
            <div className="blockdivs divsmargin">
                <TextField value={this.state.username} onKeyPress={this._handleKeyPress} onChange={this.handleUser} floatingLabelText="Username" fullWidth="true" underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}/>
            </div>
            <br/>
            <br/>
            <div align="center">
              <div className="btncont">
                <RaisedButton label="SIGN UP" primary={true} className="btncenter" onClick={this.handleSubmit}/>
              </div>  
              <br/>
              <a className="havacc"  onClick={() => window.location.replace(protocol+"://"+server+'/Login')}>Already have an account?</a>
              <br/>
            </div>
          </div>
        </div>
      </div>

      <Dialog
          title="Terms and Conditions, Privacy Policy"
          actions={actions}
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose}
          autoScrollBodyContent={true}
          contentStyle={customContentStyle}
        >   
            <iframe style={{'margin-top': '-43px', width: '100%', height: '25460px','max-height': '25460px', border: 'none'}} scrolling="no" src="https://docs.google.com/document/d/e/2PACX-1vQ_huL6WCVmfz4L78yioG5PIax_PkBQAJZ_hUUK3ZucJX25voM--1x7ItSZM-xNp6TGKo7nNP-HH6ji/pub"></iframe>
      </Dialog>
    </div>
    );
}


}

