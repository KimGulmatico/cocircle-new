import React , {Component} from 'react';
import FontIcon from 'material-ui/FontIcon';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import Avatar from 'material-ui/Avatar';
import Checkbox from 'material-ui/Checkbox'
import {blue100, blue500, blue700, grey400, darkBlack, lightBlack} from 'material-ui/styles/colors';
import './AdminAcc.css';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import Divider from 'material-ui/Divider';
import DotStat from 'mui-icons/fontawesome/circle';
import Arrow from 'mui-icons/fontawesome/chevron-down';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import NavigationMenu from 'material-ui/svg-icons/navigation/menu';
import FlatButton from 'material-ui/FlatButton';
import Popover, {PopoverAnimationVertical} from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import IconMenu from 'material-ui/IconMenu'
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert'
import AdminMan from 'material-ui/svg-icons/action/supervisor-account';
import BillIcon from 'material-ui/svg-icons/editor/monetization-on';
import SearchIcon from 'material-ui/svg-icons/action/search';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import Moreexpand from 'material-ui/svg-icons/navigation/expand-more';
import Payment from 'material-ui/svg-icons/action/payment';
import Reward from 'material-ui/svg-icons/action/card-giftcard';
import Redemption from 'material-ui/svg-icons/action/redeem';
import HistoryIcon from 'material-ui/svg-icons/action/history';
import Thumb from 'material-ui/svg-icons/action/thumb-up';
import axios from 'axios';
import renderHTML from 'react-render-html';
import Paper from 'material-ui/Paper';
import Dialog from 'material-ui/Dialog';
import {List, ListItem} from 'material-ui/List';
import CircularProgress from 'material-ui/CircularProgress';
import SecurityIcon from 'material-ui/svg-icons/hardware/security';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import Subheader from 'material-ui/Subheader';
import Box from 'mui-icons/fontawesome/th-large';
import LinearProgress from 'material-ui/LinearProgress';
import Account from 'material-ui/svg-icons/action/account-box';
import ExportBill from 'material-ui/svg-icons/action/play-for-work';
import { Collapse} from 'reactstrap';
import Check from 'material-ui/svg-icons/action/thumb-up';
import Connecton from './Connection'
import Star from 'material-ui/svg-icons/toggle/star';

const server = Connecton.server;
const serverphp = Connecton.serverphp;
const protocol = Connecton.protocol;

// const server = 'www.thecocircle.com';
// const serverphp = 'www.thecocircle.com';

const styles = {
  underlineStyle: {
    borderColor: '#9e9e9e',
  },
  labelStyle: {
    color: '#9e9e9e',
  },
  floatingLabelFocusStyle: {
    color: blue500,
  },
  hintText:{
    color: '#9e9e9e',
  },
  smallIcon: {
    width: 30,
    height: 30,
    color: '#757575',
  },
  smallIcons: {
    width: 20,
    height: 20,
    color: '#757575',
  },
  mediumIcon: {
    width: 48,
    height: 48,
    color: '#757575',
  },
  small: {
    width: 50,
    height: 50,
    padding: 0,
  },
  medium: {
    width: 96,
    height: 96,
    padding: 24,
  },
};

const customContentStyle = {
  width: '100%',
  maxWidth: '500px',
};

const dialogStyle = {
  width: 'auto',
  maxWidth: '500px',
};

const customContentStyle2 = {
  width: '100%',
  maxWidth: '650px',
};

const exportstyle = {
  width: '100%',
  padding: '20px'
};


export default class AdminAcc extends Component {

constructor(props) {
  super(props);

  this.state = {
    session: false,
    opendrawer: false,
    opendrawerdim: false,
    openpopup: false,
    title: 'Transactions',
    hideAvater: false,
    openaddadmin: false,
    opendlreward: false,
    openadditem:false,
    shadow: 1,
    acscode: null,
    show: 'Payments',
    srchcust: null,
    paperdiv: '90%',
    custID: null,
    adminID: null,
    amount: null,
    points: null,
    itemprices: null,
    itemnames: null,
    rID: null,
    cID: null,
    user: null,
    pass: null,
    pays: [],
    customers: [],
    info:[],
    reds:[], 
    redsPending: [],
    items: [],
    imagePreviewUrl: '',
    itemname: null,
    itemprice: null,
    itemdesc: null, 
    progstat: false,
    imagefile: '',
    loadersize: 40,
    loaderthick: 4,
    currencyRate: 0,
    ptsmember: null,
    tococircle: 0,
    customersprof: [],
    infos: [],
    custIDprof: null,
    srchcustprof: '',
    nameprof: '',
    paperdivprof: '90%',
    adminsCpny: [],
    transactCpny: [],
    redswithID: [],
    idtobill: null,
    disableBtnPayOK: false,
    newpasswd: null,
    cfrmpasswd: null,
    newusername: null,
    currpass1: null,
    currpass2: null,
  };
  this.handlePayments = this.handlePayments.bind(this);
  this.handleRedeemPoints = this.handleRedeemPoints.bind(this);
  this.handlePopupMore = this.handlePopupMore.bind(this);
  this.handleClosePopupMore = this.handleClosePopupMore.bind(this);
  this.handleKeyDownSrch = this.handleKeyDownSrch.bind(this);
  this.handleInsertPayment = this.handleInsertPayment.bind(this);
  this.handleGetUserInfo = this.handleGetUserInfo.bind(this);
  this.handleInsertRedeem = this.handleInsertRedeem.bind(this);
  this.handleLoadRedeem = this.handleLoadRedeem.bind(this);
  this.handlePendingRedemptions = this.handlePendingRedemptions.bind(this);
  this.handleLoadRedeemPending = this.handleLoadRedeemPending.bind(this);
  this.handleLoadRedeemHistory = this.handleLoadRedeemHistory.bind(this);
  this.handleDelivered = this.handleDelivered.bind(this);
  this.handleCancel = this.handleCancel.bind(this);
  this.handleLoadItems = this.handleLoadItems.bind(this);
  this.handleInsertItem = this.handleInsertItem.bind(this);
  this.handleItems = this.handleItems.bind(this);
  this._openFileDialog = this._openFileDialog.bind(this);
  this.handleRates = this.handleRates.bind(this);
  this.handleAmountKeyup = this.handleAmountKeyup.bind(this);
  this.handleCustomers = this.handleCustomers.bind(this);
  this.handleKeyDownSearching = this.handleKeyDownSearching.bind(this);
  this.hideDpdown = this.hideDpdown.bind(this);
  this.handleGetUserProfileProf = this.handleGetUserProfileProf.bind(this);
  this.srchChangeForProf = this.srchChangeForProf.bind(this);
  this.handleBilling = this.handleBilling.bind(this);
  this.handleLoadAdminsCpny = this.handleLoadAdminsCpny.bind(this);
  this.handleLoadTransWithID = this.handleLoadTransWithID.bind(this);
  this.handleBilled = this.handleBilled.bind(this);
  this.handleOpenBilling = this.handleOpenBilling.bind(this);
  this.handleSecurity = this.handleSecurity.bind(this)
  this.handleNewPasswd = this.handleNewPasswd.bind(this)
  this.handleCfrmPasswd = this.handleCfrmPasswd.bind(this)
  this.handleNewUsername = this.handleNewUsername.bind(this)
  this.handleCurrentPass1 = this.handleCurrentPass1.bind(this)
  this.handleCurrentPass2 = this.handleCurrentPass2.bind(this)
  this.handleUpdatePassword = this.handleUpdatePassword.bind(this)
  this.handleUpdateUsername = this.handleUpdateUsername.bind(this)
}

handleToggle = () => this.setState({opendrawer: !this.state.opendrawer});

handleToggleDim = () => this.setState({opendrawerdim: !this.state.open});

handleCloseDim = () => this.setState({opendrawerdim: false});

updateDimensions() {
  if(window.innerWidth < 769) {
    this.setState({opendrawer:false});
    this.setState({hideAvater:true});
    this.setState({shadow:2});
    this.setState({loadersize: 50, loaderthick: 4,});
    this.setState({paperdivprof: document.body.clientWidth});
  }
  else{
    this.setState({opendrawer:true});
    this.setState({opendrawerdim:false});
    this.setState({hideAvater:false});
    this.setState({shadow:0});
    this.setState({loadersize: 80, loaderthick: 5,});
    this.setState({paperdivprof: '110%'});
  } 
  //var widthsrch = document.getElementById("srchfield").clientWidth;
  //this.setState({paperdiv: widthsrch});
}

componentDidMount() {
  this.interval = setInterval(() => this.handleCheckSession(), 1000);
  this.handleGetUserInfo();
  this.interval = setInterval(()=> this.handleLoadRedeemPending(),1000);
  this.interval = setInterval(()=> this.handleLoadAdminsCpny(),1000);
  this.interval = setInterval(()=> this.handleLoadRedeemHistory(),1000);
  this.interval = setInterval(()=> this.handleLoadTransWithID(),1000);
  //this.handleCheckSession();
  this.updateDimensions();
  window.addEventListener("resize", this.updateDimensions.bind(this));
  this.handleLoadPayments();
  //this.handleLoadRedeem();
  //this.handleLoadRedeemPending();
  //this.handleLoadRedeemHistory();
  this.handleRates()
  //this.handleLoadAdminsCpny();
  
}

componentWillUnmount() {
  window.removeEventListener("resize", this.updateDimensions.bind(this));
}

handlePayments(){
  //this.handleLoadPayments();
  this.handleCloseDim();
  this.setState({title: 'Transactions', maHide: '', show: 'Payments'});
}

handleRedeemPoints(){
  this.handleCloseDim();
  this.setState({title: 'Redeem History', maHide: 'hidden', show: 'Redeem'});
}

handlePendingRedemptions(){
  this.handleCloseDim();
  this.setState({title: 'Pending Redemptions', maHide: 'hidden', show: 'Pending'});
}

handleItems(){
  this.handleCloseDim();
  this.setState({title: 'Items', maHide: 'hidden', show: 'Items'});

}

handleCustomers(){
  this.handleCloseDim();
  this.setState({title: 'Customers', maHide: 'hidden', show: 'Customers'});

}

handleBilling(){
  this.handleCloseDim();
  this.setState({title: 'Billing Informations', maHide: 'hidden', show: 'Billing'});
}

handleSecurity() {
  this.handleCloseDim();
  this.setState({title: 'Security'});
  this.setState({maHide: 'hidden'});
  this.setState({show: 'Security'});
}

handlePopupMore(event){
  event.preventDefault();
  this.setState({
      openpopup: true,
      anchorEl: event.currentTarget,
  });
};

handleClosePopupMore(){
  this.setState({
  openpopup: false,
  });
};

handleOpenAddAdmin = () => {
  //this.handleCreatePayment();
  this.setState({openaddadmin: true, srchcust: '', amount: ''});
};

handleOpenRewards = () => {
  //this.handleCreatePayment();
  this.setState({opendlreward: true, srchcust: '', itemnames: '', itemprices: '', points: ''});
};

handleCloseAddAdmin  = () => {
  this.setState({openaddadmin: false});
};

handleCloseRewards  = () => {
  this.setState({opendlreward: false});
};

handleOpenAddItem = () => {
  this.setState({openadditem: true});
};

handleCloseAddItem  = () => {
  this.setState({openadditem: false});
};

handleLoadPayments(){
  var user = localStorage.getItem('user');
  var pass = localStorage.getItem('pass');
  axios.get(protocol+'://'+serverphp+'/php/loadPayments.php?user='+user+'&pass='+pass)
      .then(res => {
         var info = JSON.parse(JSON.stringify(res.data));
  
         this.setState({pays: info});
         
      });
}
//Searching
handleSearchCustomerChange = (event) => {
    this.setState({
      srchcust: event.target.value,
    });
};

handleKeyDownSrch(event){
    axios.get(protocol+'://'+serverphp+'/php/searchCust.php?x='+event.target.value)
      .then(res => {
        var customers = JSON.parse(JSON.stringify(res.data));
        this.setState({customers: customers});
        if(this.state.customers.length !== 0){
          document.getElementById("dropdown").style.display = "block";
        }else{
          document.getElementById("dropdown").style.display = "none";
        }
      });
}

hideDropdown(id, name, points){
  //alert(points);
  this.setState({custID: id, srchcust: name, points:points});
  document.getElementById("dropdown").style.display = "none";
}

handleAmount = (event) => {
  if(event.target.value != ""){
    this.setState({
      amount: parseFloat(event.target.value),
    });
  }else{
    this.setState({
      amount: event.target.value,
    });
  }
};

handlePoints = (event) => {
    this.setState({
      ptsmember: event.target.value
    });
}

handlePointsText = (event) => {
    this.setState({
      points: event.target.value,
    });
};

handleItemPriceText= (event) => {
    this.setState({
      itemprices: event.target.value,
    });
};

handleItemNameText= (event) => {
    this.setState({
      itemnames: event.target.value,
    });
};

handleInsertPayment(){
  this.setState({disableBtnPayOK: true});
  axios.get(protocol+'://'+serverphp+'/php/insertPayment.php?user='+this.state.user+'&pass='+this.state.pass+'&cID='+this.state.custID+'&amount='+this.state.amount+'&points='+this.state.ptsmember+'&HKDrate='+this.state.currencyRate+'&toCocircle='+this.state.tococircle)
      .then(res => {
        this.handleCloseAddAdmin();
        this.setState({amount:null, srchcust: null, ptsmember: '', tococircle: '', disableBtnPayOK: false});
        this.handleLoadPayments();
        this.handleLoadAdminsCpny();
        alert(res.data);
      });
}

handleGetUserInfo(){
  var user = localStorage.getItem('user');
  var pass = localStorage.getItem('pass');
  axios.get(protocol+'://'+serverphp+'/php/getInfoAdmin.php?user='+user+'&pass='+pass)
      .then(res => {
         var info = JSON.parse(JSON.stringify(res.data));
         this.setState({info: info, adminID: info[0].id});
         this.handleLoadItems();
         this.handleLoadRedeemHistoryWithID();
         this.handleLoadTransWithID();
         console.log(this.state.info[0].currency)
      });
}

handleInsertRedeem(){
  axios.get(protocol+'://'+serverphp+'/php/insertRedeem.php?x='+JSON.stringify(this.state))
      .then(res => {
         //alert(res.data);
         if(typeof res.data == 'number'){
         this.setState({points: res.data, itemnames: "", itemprices: ""});
         }
         this.handleLoadRedeem();
      });
}

//not used
handleLoadRedeem() {
  var user = localStorage.getItem('user');
  var pass = localStorage.getItem('pass');
  axios.get(protocol+'://'+serverphp+'/php/loadRedeem.php?user='+user+'&pass='+pass)
    .then(res => {
    var redeem = JSON.parse(JSON.stringify(res.data));
    this.setState({reds: redeem});
  });

}

handleLoadRedeemPending() {
  var user = localStorage.getItem('user');
  var pass = localStorage.getItem('pass');
  axios.get(protocol+'://'+serverphp+'/php/loadRedeemPending.php?user='+user+'&pass='+pass)
    .then(res => {
    var rdpend = JSON.parse(JSON.stringify(res.data));
  
    if(rdpend.length != this.state.redsPending.length){
      this.setState({redsPending: rdpend});
    }

  });

}

handleLoadRedeemHistory() {
  var user = localStorage.getItem('user');
  var pass = localStorage.getItem('pass');
  axios.get(protocol+'://'+serverphp+'/php/loadRedeemHistory.php?user='+user+'&pass='+pass)
    .then(res => {
    var rdhst = JSON.parse(JSON.stringify(res.data));
    if(rdhst.length != this.state.reds.length){
    this.setState({reds: rdhst});
    }
  });

}

handleDelivered(idval, cid, price) {
  var user = localStorage.getItem('user');
  var pass = localStorage.getItem('pass');

  axios.get(protocol+'://'+serverphp+'/php/trullyredeem.php?user='+user+'&pass='+pass+'&rid='+idval+'&cID='+cid+'&price='+price)
    .then(res => {
    this.handleLoadRedeemPending();
    this.handleLoadRedeemHistory();
    this.handleLoadAdminsCpny();
    this.handleLoadRedeemHistoryWithID();
    this.handleLoadTransWithID();
    alert(res.data);
  });

}

handleCancel(idval, cid, price) {
  var user = localStorage.getItem('user');
  var pass = localStorage.getItem('pass');
  axios.get(protocol+'://'+serverphp+'/php/cancelredeem.php?user='+user+'&pass='+pass+'&rid='+idval+'&cID='+cid+'&price='+price)
    .then(res => {
    this.handleLoadRedeemPending();
    this.handleLoadRedeemHistory();
    alert(res.data);
  });

}

handleCheckSession(){
  var user = localStorage.getItem('user');
  var pass = localStorage.getItem('pass');
  if(this.state.user == null && this.state.pass == null){
    this.setState({user: user});
    this.setState({pass: pass});
  }
  if(user != "" && pass != ""){
  axios.get(protocol+'://'+serverphp+'/php/checkSessionLogin.php?user='+user+'&pass='+pass)
    .then(res => {
        if(res.data.trim() == 'admin'){
          if(this.state.session != true){
          this.setState({session: true});
          }
        }
        else if(res.data.trim() == 'superadmin'){
          window.location.replace(protocol+'://'+server+'/SuperAdmin');
        }
        else if(res.data.trim() == 'C'){
          window.location.replace(protocol+'://'+server+'/User');
        }
        else{
          window.location.replace(protocol+'://'+server+'/Login');
        }
  });
  }
  else{
    window.location.replace(protocol+'://'+server+'/Login');
  }
}

handleSignout(){
  var user = localStorage.getItem('user');
  var pass = localStorage.getItem('pass');
  axios.get(protocol+'://'+serverphp+'/php/signout.php?user='+user+'&pass='+pass)
    .then(res => {
        localStorage.setItem('user','');
        localStorage.setItem('pass','');
  });
}  

handleInsertItem(){
  // axios.get(protocol+'://'+serverphp+'/php/insertItem.php?img='+this.state.imagefile+'&user='+this.state.user+'&pass='+this.state.pass+'&name='+this.state.itemname+'&price='+this.state.itemprice+'&desc='+this.state.itemdesc)
       //.then(res => {
         //this.handleCloseAddItem();
         //this.setState({itemname:null, itemdesc: null, itempoints: null});
         //this.handleLoadPayments();
         //alert(res.data);
       //});
   this.setState({progstat: true});
   const data = new FormData();
   data.append('user', this.state.user);
   data.append('pass', this.state.pass);
   data.append('name', this.state.itemname);
   data.append('desc', this.state.itemdesc);
   data.append('price', this.state.itemprice);
   data.append('file', this.state.imagefile);
   data.append('adminID', this.state.adminID);
   axios.post(protocol+'://'+serverphp+'/php/insertItem.php', data)
   .then(res => {
   this.setState({progstat: false, imagePreviewUrl: '', itemdesc: '', itemname: '', itemprice: ''});
   //alert(res.data);
   this.handleLoadItems();
   this.handleCloseAddItem();
   });
 
 }
 
 handleLoadItems(){
   const data = new FormData();
   data.append('adminID', this.state.adminID);
   axios.post(protocol+'://'+serverphp+'/php/loadItems.php', data)
     .then(res => {
      var items = JSON.parse(JSON.stringify(res.data));
      this.setState({items: items});
   });
 };

 _handleImageChange(e) {
  e.preventDefault();
  
  let reader = new FileReader();
  let file = e.target.files[0];

  reader.onloadend = () => {
    this.setState({
      imagefile: file,
      imagePreviewUrl: reader.result
    });
  }
  document.getElementById('picname').innerHTML = e.target.files[0].name;
  reader.readAsDataURL(file)
}

_openFileDialog(){
  this.inputElement.click();
}

handleChangeItemName = (event) => {
  this.setState({
    itemname: event.target.value,
  });
};

handleChangeItemPrice = (event) => {
  this.setState({
    itemprice: event.target.value,
  });
};

handleChangeItemDesc = (event) => {
  this.setState({
    itemdesc: event.target.value,
  });
};

handleRates(event){
  axios.get(protocol+'://openexchangerates.org/api/latest.json?app_id=87f752db5cad439da8c69a3f3ca2cf64')
    .then(res => {
      
      var ratesjson = JSON.parse(JSON.stringify(res.data));    
      this.setState({rates: ratesjson}); 
  });
}

handleAmountKeyup(event){
  var adminCurrency = this.state.info[0].currency;
  var rate = parseFloat(this.state.rates.rates[adminCurrency]);    
  this.setState({currencyRate: rate});

  var points = Math.round(parseFloat(this.state.amount) / rate);
  const cent = 100;
  var tococircle = points / cent;
  
  isNaN(tococircle)?this.setState({ptsmember: 0, tococircle: 0}):this.setState({ptsmember: points, tococircle: tococircle});
}

//Searching

srchChangeForProf(event){
  this.setState({srchcustprof: event.target.value});
}



handleKeyDownSearching(event){
  axios.get(protocol+'://'+serverphp+'/php/searchCust.php?x='+event.target.value+'&adminID='+this.state.adminID)
      .then(res => {
        var customers = JSON.parse(JSON.stringify(res.data));
        this.setState({customersprof: customers}); 
        if(this.state.customersprof.length !== 0){
          document.getElementById("dropdowns").style.display = "block";
        }else{
          document.getElementById("dropdowns").style.display = "none";
        }
      });
}

hideDpdown(id, name){
  //alert(points);
  this.setState({custIDprof: id, srchcustprof: name});
  document.getElementById("dropdowns").style.display = "none";
  this.handleGetUserProfileProf(id);
}

handleGetUserProfileProf(id){
  axios.get(protocol+'://'+serverphp+'/php/getUserInfoIDSrch.php?id='+id)
      .then(res => {
         var info = JSON.parse(JSON.stringify(res.data));
         this.setState({infos: info});
      });
}

handleLoadAdminsCpny() {
  var user = localStorage.getItem('user');
  var pass = localStorage.getItem('pass');
  axios.get(protocol+'://'+serverphp+'/php/loadAdminCpnyOne.php?user='+user+'&pass='+pass)
    .then(res => {
     var admins = JSON.parse(JSON.stringify(res.data));
     this.setState({adminsCpny: admins});
  });
}

handleLoadTransWithID() {
  axios.get(protocol+'://'+serverphp+'/php/loadTransactionWithID.php?adminID='+this.state.adminID)
    .then(res => {
     var trans = JSON.parse(JSON.stringify(res.data));
     this.setState({transactCpny: trans});
  });
}

handleLoadRedeemHistoryWithID() {
  axios.get(protocol+'://'+serverphp+'/php/loadRedeemHistorySAWithID.php?adminID='+this.state.adminID)
    .then(res => {
    var rdhst = JSON.parse(JSON.stringify(res.data));
    this.setState({redswithID: rdhst});
  });

}

handleBilled() {
  axios.get(protocol+'://'+serverphp+'/php/updateBilling.php?adminID='+this.state.idtobill)
    .then(res => {
     this.handleLoadAdminsCpny();
     this.handleLoadTransWithID();
     this.handleLoadRedeemHistoryWithID();
     this.setState({openbilling: false});
  });
}

handleOpenBilling(id){
  this.setState({openbilling: true, idtobill: id});
}

handleNewPasswd = (event) => {
  this.setState({
    newpasswd: event.target.value,
  });
};

handleCfrmPasswd = (event) => {
  this.setState({
    cfrmpasswd: event.target.value,
  });
};

handleNewUsername = (event) => {
  this.setState({
    newusername: event.target.value,
  });
};

handleCurrentPass1 = (event) => {
  this.setState({
    currpass1: event.target.value,
  });
};

handleCurrentPass2 = (event) => {
  this.setState({
    currpass2: event.target.value,
  });
};

handleUpdatePassword(){
  
  axios.post(protocol+'://'+serverphp+'/php/updatePassword.php?user='+this.state.user+'&pass='+this.state.newpasswd+'&cfrmpass='+this.state.cfrmpasswd+'&oldpass='+this.state.currpass1)
  .then(res => {
    if(res.data.trim() == "changed"){
      localStorage.setItem('pass',this.state.newpasswd);
      this.setState({pass: this.state.newpasswd,
                     newpasswd: '',
                     cfrmpasswd: '',
                     currpass1: ''})
      alert('Updated');
    }
    else {
      alert(res.data);
    }
  });

}

handleUpdateUsername(){
  
  axios.post(protocol+'://'+serverphp+'/php/updateUsername.php?user='+this.state.user+'&newuser='+this.state.newusername+'&oldpass='+this.state.currpass2)
  .then(res => {
    if(res.data.trim() == "changed"){
      localStorage.setItem('user',this.state.newusername);
      this.setState({user: this.state.newusername,
                     newusername: '',
                     currpass2: ''})
      alert('Updated');
    }
    else {
      alert(res.data);
    }
  });

}

render(){
    let {imagePreviewUrl} = this.state;
    let $imagePreview = null;
    if (imagePreviewUrl) {
      $imagePreview = (<div><img src={imagePreviewUrl} width="100%" height="255px" /></div>);
    } else {
      $imagePreview = (<div><img src={require('./images/addimage.png')} width="100%" height="255px" /></div>);
    }

    function RightIconMenu(props){
      const iconButtonElement = (
        <IconButton
          touch={true}
          tooltip="more"
          tooltipPosition="bottom-left"
        >
          <MoreVertIcon color={grey400} />
        </IconButton>
      );
      return(
      <IconMenu iconButtonElement={iconButtonElement} style={{position: 'absolute','right': '7px', 'margin-top':'-12px'}}>
        <MenuItem onClick={()=>props.get.handleDelivered(props.id, props.cid, props.price)}>Delivered</MenuItem>
        <MenuItem onClick={()=>props.get.handleCancel(props.id, props.cid, props.price)}>Cancel</MenuItem>
      </IconMenu>);
    };

      var showhat;
      if(this.state.show == "Payments"){
        showhat = (
        <div style={{height: '100%'}}>
        <Paper  zDepth={1} style={{height: '100%'}}>
                  <div>
                    <Subheader style={{'font-weight': '700', 'background-color': '#fafafa'}}>TRANSACTIONS</Subheader>
                    <Divider/>
                    <List>
                    {(this.state.pays.length != 0)?this.state.pays.map((rd) => (
                    <div>
                    <ListItem
                      leftAvatar={<Avatar src={rd.cpic} />}
                      primaryText={rd.cname+'(Customer)'}
                      secondaryText={
                        <p>
                          <span style={{color: darkBlack}}>{rd.aname+'(Operator)'}</span><br/>
                          {rd.desc.replace(/[\u00A0-\u9999<>\&]/gim, function(i) {
                              return '&#'+i.charCodeAt(0)+';';
                            })}
                        </p>
                      }
                      secondaryTextLines={2}
                    />
                    <Divider inset={true} />
                    </div>
                    )):<p style={{padding: '15px'}}>Nothing to see.</p>}
                    </List>
                   
                  </div>
                </Paper> 
        <FloatingActionButton className="Floatingbtn" onClick={this.handleOpenAddAdmin }>
            <Payment />
        </FloatingActionButton> 
        </div>
);
      }
      else if(this.state.show == "Pending"){
           showhat = (
              <div style={{height: '100%'}}>
              <Paper zDepth={1} style={{height: '100%'}}>
                  <div>
                    <List>
                    {(this.state.adminID != null && this.state.redsPending.length != 0)?this.state.redsPending.map((rd) =>  (
                  
                    <div>
                    <ListItem
                      leftAvatar={<Avatar src={rd.pic} />}
                      rightIconButton={<IconMenu iconButtonElement={<IconButton
                        touch={true}
                        tooltip="more"
                        tooltipPosition="bottom-left"
                      >
                        <MoreVertIcon color={grey400} />
                      </IconButton>} style={{position: 'absolute','right': '7px', 'margin-top':'-12px'}}>
                      <MenuItem onClick={()=>this.handleDelivered(rd.id, rd.cID, rd.iprice)}>Delivered</MenuItem>
                      <MenuItem onClick={()=>this.handleCancel(rd.id, rd.cID, rd.iprice)}>Cancel</MenuItem>
                    </IconMenu>}
                      primaryText={rd.cname}
                      secondaryText={
                        <p>
                          <span style={{color: darkBlack}}>{rd.desc}</span><br/>
                          {rd.desc2}
                        </p>
                      }
                      secondaryTextLines={2}
                    />
                    <Divider inset={true} />
                    </div>
                    )):<p style={{padding: '15px'}}>Nothing to see.</p>}
                    </List>
                   
                  </div>
                </Paper> 
            </div>);
      }
      else if(this.state.show == "Redeem"){
        showhat = (
        <div key="mainRedeemDiv" style={{height: '100%'}}>
              <Paper zDepth={1} style={{height: '100%'}}>
                  <div>
                    <List key="MainlistRedeem">
                    {(this.state.adminID != null && this.state.reds.length != 0)?this.state.reds.map((rd) => (
                    <div>
                    <ListItem
                      key={'lirhistory'+rd.id}
                      leftAvatar={<Avatar key={'lirhistoryavatar'+rd.id} src={rd.pic} />}
                      primaryText={rd.cname}
                      secondaryText={
                        <p key={'lirhistorydesc2'+rd.id}>
                          <span key={'lirhistorydesc'+rd.id} style={{color: darkBlack}}>{rd.desc}</span><br/>
                          {rd.desc2}
                        </p>
                      }
                      secondaryTextLines={2}
                    />
                    <Divider inset={true} />
                    </div>
                    )):<p style={{padding: '15px'}}>Nothing to see.</p>}
                    </List>
                   
                  </div>
                </Paper> 
          <FloatingActionButton className="Floatingbtn" onClick={this.handleOpenRewards } hidden>
            <Reward />
          </FloatingActionButton> 
        </div>);
      }
      else if(this.state.show == "Items"){
        const {items} = this.state;
        showhat = (<div>
              <Paper zDepth={1} style={{height: '100%'}}>
                   
                  <div>
                    <Subheader style={{'font-weight': '700', 'background-color': '#fafafa'}}>ITEMS ADDED</Subheader>
                    <Divider/>
                    <List>
                    {(this.state.items.length != 0)?items.map((items) => (
                    <div>
                    <ListItem
                      leftAvatar={<Avatar src={protocol+'://'+serverphp+'/php/Items/'+items.imagename} />}
                      primaryText={items.name+' '+items.price+' pts'}
                      secondaryText={
                        <p>
                          <span style={{color: darkBlack}}>{items.desc}</span>
                        </p>
                      }
                      secondaryTextLines={2}
                    />
                    <Divider inset={true} />
                    </div>
                    )):<p style={{padding: '15px'}}>Nothing to see.</p>}
                    </List>
                   
                  </div>
                  
                </Paper>
                <FloatingActionButton className="Floatingbtn" onClick={this.handleOpenAddItem }>
                  <ContentAdd />
                </FloatingActionButton>
                </div>);
      }
      else if(this.state.show == "Customers"){      
        showhat = (<div style={{width: '100%', 'min-height': '100vh', overflow: 'hidden'}}>
              <Paper zDepth={1}  style={{padding: '0px 10px 0px 10px', 'max-width': '400px', 'margin-left': 'auto', 'margin-right': 'auto'}} >
                        <div>
                        <div className="row">
                        <div className="col-sm-12">   
                          <div className="row">    
                          <div className="col">    
                          <TextField key="new" style={{'font-size': '20px'}} hintText="Search" underlineShow={false} fullWidth={true} hintStyle={styles.hintText} onChange={this.srchChangeForProf} value={this.state.srchcustprof} onKeyUp={this.handleKeyDownSearching}/> 
                          <Paper align="left" id="dropdowns" zDepth={1} style={{width: this.state.paperdivprof, 'max-width': '400px','margin': '0 -10px 0 -10px'}} className="dropdownsrch">
                            <List>
                              {(this.state.customersprof.length != 0)?this.state.customersprof.map((customers) => (
                                  <ListItem primaryText={customers.name} onClick={() => this.hideDpdown(customers.id, customers.name)}/>
                                )):<div>Empty</div>}
                            </List>
                          </Paper>
                          </div>
                          <div className="col-1">
                          <SearchIcon style={{width: 30, height: 30, color: '#9e9e9e', 'margin-top':'10px','position':'absolute', right: '10px'}}/>
                          </div>
                          </div>
                        </div>
                        </div>
                        </div>
                </Paper>
                <div>
                {this.state.infos.length != 0?
                      <div style={{padding: '7.5px'}}>
                <Paper zDepth={1} className="paperCon" align="center" style={{padding: '20px'}}>
                  <Avatar src={this.state.infos[0].lnkPicUrl} size={125}/>
                  <br/>
                  <br/>
                  <h4 className="primarytxt">{this.state.infos[0].fname+' '+this.state.infos[0].lname}</h4>
                </Paper>
                <Paper zDepth={1} className="paperCon" style={{overflow:'hidden'}}>
                  <Subheader style={{'font-weight': '700', 'background-color': '#fafafa'}}>PERSONAL INFO</Subheader>
                  <Divider />
                  <div className="container">
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Firstname:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField hintText="Firstname" underlineShow={false}  fullWidth={true} value={this.state.infos[0].fname}/>
                  </div>
                  </div>
                   <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Lastname:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField hintText="Lastname" underlineShow={false} fullWidth={true} value={this.state.infos[0].lname}/>
                  </div>
                  </div>
                  <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Birthday:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField hintText="Birthday" underlineShow={false}  fullWidth={true} value={this.state.infos[0].dob}/>
                  </div>
                  </div>
                  <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Email:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField hintText="Email" underlineShow={false}  fullWidth={true} value={this.state.infos[0].email}/>
                  </div>
                  </div>
                  <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Mobile:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField hintText="Mobile" underlineShow={false}  fullWidth={true} value={this.state.infos[0].mobile}/>
                  </div>
                  </div>
                  </div>
                </Paper>

                <Paper zDepth={1} className="paperCon" style={{overflow:'hidden'}}>
                  <Subheader style={{'font-weight': '700', 'background-color': '#fafafa'}}>LINKEDIN INFO</Subheader>
                  <Divider />
                  <div className="container">
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Headline:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField hintText="Headline" underlineShow={false}  fullWidth={true} value={this.state.infos[0].lnkHeadline}/>
                  </div>
                  </div>
                   <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Profile URL:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField hintText="Profile URL" underlineShow={false} fullWidth={true} value={this.state.infos[0].lnkProfileUrl}/>
                  </div>
                  </div>
                  <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Location:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField hintText="Location" underlineShow={false}  fullWidth={true} value={this.state.infos[0].lnkLocation}/>
                  </div>
                  </div>
                  <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Summary:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField hintText="Summary" underlineShow={false}  fullWidth={true} value={this.state.infos[0].lnkSummary}/>
                  </div>
                  </div>
                  <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Specialties:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField hintText="Specialties" underlineShow={false}  fullWidth={true} value={this.state.infos[0].lnkSpecialties}/>
                  </div>
                  </div>
                  </div>
                </Paper>

                <Paper zDepth={1} className="paperCon" style={{overflow:'hidden'}}>
                  <Subheader style={{'font-weight': '700', 'background-color': '#fafafa'}}>COMPANY INFO</Subheader>
                  <Divider />
                  <div className="container">
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Name:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField hintText="Name" underlineShow={false}  fullWidth={true} value={this.state.infos[0].cpnyName}/>
                  </div>
                  </div>
                   <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Sector:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField hintText="Sector" underlineShow={false} fullWidth={true} value={this.state.infos[0].cpnySector}/>
                  </div>
                  </div>
                  <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Size:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField hintText="Size" underlineShow={false}  fullWidth={true} value={this.state.infos[0].cpnySize}/>
                  </div>
                  </div>
                  <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Start date:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField hintText="Start date" underlineShow={false}  fullWidth={true} value={this.state.infos[0].cpnyStartDate}/>
                  </div>
                  </div>
                  <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Summary:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField hintText="Summary" underlineShow={false}  fullWidth={true} value={this.state.infos[0].cpnySummary}/>
                  </div>
                  </div>
                  <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Location:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField hintText="Location" underlineShow={false}  fullWidth={true} value={this.state.infos[0].cpnyLocation}/>
                  </div>
                  </div>
                  <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Contact:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField hintText="Contact" underlineShow={false}  fullWidth={true} value={this.state.infos[0].cpnyContact}/>
                  </div>
                  </div>
                  <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Website:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField hintText="Website" underlineShow={false}  fullWidth={true} value={this.state.infos[0].cpnyWeb}/>
                  </div>
                  </div>
                  <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Title:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField hintText="Summary" underlineShow={false}  fullWidth={true} value={this.state.infos[0].cpnyTitle}/>
                  </div>
                  </div>
                  <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Description:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField hintText="Description" underlineShow={false}  fullWidth={true} value={this.state.infos[0].cpnyDesc}/>
                  </div>
                  </div>
                  <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Logo URL:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField hintText="Logo URL" underlineShow={false}  fullWidth={true} value={this.state.infos[0].cpnyLogo}/>
                  </div>
                  </div>
                  </div>
                </Paper>

                </div>
                :''}
                  </div>

                </div>);
      }
      else if(this.state.show == "Billing"){    
        const {billinfo} = this.state;
        showhat = (<div>
                     {(this.state.adminsCpny.length != 0)?this.state.adminsCpny.map((ad) => (        
                    <Paper zDepth={1} style={{height: 'auto'}}>
                      <div>
                      <div className="container">
                      <div className="row">
                            <div className="col">
                            <p style={{'font-weight': '700', 'font-size': '15px', color: '#757575', 'margin-top':'13px'}}>{ad.cpnyname}</p>
                            </div>
                            <div style={{width: '50px', 'float': 'right'}}>
                            <IconButton hidden={(ad.stat == "pos" || ad.stat == "zero")?true:false} onClick={()=>this.handleOpenBilling(ad.id)} tooltipPosition="bottom-right" style={styles.small} iconStyle={styles.smallIcons}>
                              <Check/>
                            </IconButton>   
                            </div>
                            <div style={{width: '50px', 'float': 'right'}}>  
                            <IconButton onClick={()=>{window.open(protocol+'://'+serverphp+'/php/exportBilling2.php?id='+ad.id+'&cpny='+ad.cpnyname, '_blank')}} tooltipPosition="bottom-right" style={styles.small} iconStyle={styles.smallIcon}>
                              <ExportBill/>
                            </IconButton>
                            </div>
                      </div>
                      <div className="row">
                            <div className="col">
                            <p style={{'font-size': '13px', 'margin-top': '-14px'}}>{ad.cpnyadrs}</p>
                            <p style={{'font-size': '13px', 'margin-top': '-14px'}}>{ad.name}(Operator)</p>
                            </div>
                      </div>
                      <div className="row">
                            <div className="col">
                            <p style={{'font-weight': '700', 'font-size': '15px', color: '#757575'}}>Total Points Issued:</p>
                            </div>
                            <div style={{width: '100px'}}>
                            <p style={{'font-weight': '700', float: 'right', color: blue500, 'margin-right': '18px', 'font-size': '15px'}}>{ad.tpi} pts</p>
                            </div>
                      </div>
                      <div className="row">
                            <div className="col">
                            <p style={{'font-weight': '700', 'font-size': '15px', color: '#757575'}}>Total Points Redeemed:</p>
                            </div>
                            <div style={{width: '100px'}}>
                            <p style={{'font-weight': '700', float: 'right', 'margin-right': '18px', 'font-size': '15px', color: '#F44336'}}>{ad.tps} pts</p>
                            </div>
                      </div>
                      <div className="row">
                            <div className="col">
                            <p style={{'font-weight': '700', 'font-size': '15px', color: '#757575'}}>Share by Admin:</p>
                            </div>
                            <div style={{width: '100px'}}>
                            <p style={{'font-weight': '700', float: 'right', 'margin-right': '18px', 'font-size': '15px', color: '#F44336'}}>{ad.share} %</p>
                            </div>
                      </div>
                      <div className="row">
                            <div className="col">
                            <p style={{'font-weight': '700', 'font-size': '15px', color: '#757575'}}>Share by The Cocircle:</p>
                            </div>
                            <div style={{width: '100px'}}>
                            <p style={{'font-weight': '700', float: 'right', 'margin-right': '18px', 'font-size': '15px', color: '#F44336'}}>{ad.ccshare} %</p>
                            </div>
                      </div>
                      <div className="row">
                            <div className="col">
                            <p style={{'font-weight': '700', 'font-size': '15px', color: '#757575'}}>Total:</p>
                            </div>
                            <div style={{width: '250px'}}>
                            <p style={{'font-weight': '700', float: 'right', 'margin-right': '18px', 'font-size': '15px', color: '#4CAF50'}}>{Math.abs(ad.totalpts)} pts {(ad.stat == "neg")?"from":""} {(ad.stat == "zero")?"":""} {(ad.stat == "pos")?"to":""} {(ad.stat != "zero")?"The Cocircle":""} </p>
                            </div>
                      </div>
                      </div>
                      <Collapse isOpen={true}>
                      <Divider />
                      <Subheader style={{'font-weight': '700', 'background-color': '#fafafa'}}>POINTS ISSUED</Subheader>
                      <Divider />
                      <List>
                      {(this.state.transactCpny.length != 0)?this.state.transactCpny.map((rd) => (
                      <div>
                      <ListItem
                        leftAvatar={<Avatar src={rd.cpic} />}
                        primaryText={rd.cname+'(Customer)'}
                        secondaryText={
                          <p>
                            <span style={{color: darkBlack}}>{rd.aname+'(Operator)'}</span><br/>
                            {rd.desc.replace(/[\u00A0-\u9999<>\&]/gim, function(i) {
                                return '&#'+i.charCodeAt(0)+';';
                              })}
                          </p>
                        }
                        secondaryTextLines={2}
                      />
                      <Divider inset={true} />
                      </div>
                      )):<p style={{padding: '15px'}}>Nothing to see.</p>}
                      </List>
                      <Divider />
                      <Subheader style={{'font-weight': '700', 'background-color': '#fafafa'}}>REDEMPTIONS</Subheader>
                      <Divider />
                      <List key="MainlistRedeem">
                      {(this.state.redswithID.length != 0)?this.state.redswithID.map((rd) => (
                      <div>
                      <ListItem
                        key={'lirhistory'+rd.id}
                        leftAvatar={<Avatar key={'lirhistoryavatar'+rd.id} src={rd.pic} />}
                        primaryText={rd.cname+'(Customer)'}
                        secondaryText={
                          <p key={'lirhistorydesc2'+rd.id}>
                            <span key={'lirhistorydesc'+rd.id} style={{color: darkBlack}}>{rd.desc}</span><br/>
                            {rd.desc2}
                          </p>
                        }
                        secondaryTextLines={2}
                      />
                      <Divider inset={true} />
                      </div>
                      )):<p style={{padding: '15px'}}>Nothing to see.</p>}
                      </List>
                      </Collapse>
                    </div>
                  </Paper> 
                  )):<p style={{padding: '15px'}}>Nothing to see.</p>}
                  </div>);
      }
      else if(this.state.show == "Security"){
        showhat = (
          <div style={{height: '100%'}}>
                <div className="row">
                  <div className="col-sm-6">
                    <Paper style={exportstyle} zDepth={1}>
                      <h4>Update password</h4>
                      <br/>
                      <TextField type="password" floatingLabelText="Current Password" value={this.state.currpass1} onChange={this.handleCurrentPass1} underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}/>
                      <br/>
                      <TextField type="password" floatingLabelText="New password" value={this.state.newpasswd} onChange={this.handleNewPasswd} underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}/>
                      <br/>
                      <TextField type="password" floatingLabelText="Confirm password" value={this.state.cfrmpasswd} onChange={this.handleCfrmPasswd} underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}/>
                      <br/>
                      <br/>
                      <RaisedButton label="Update" primary={true} onClick={this.handleUpdatePassword}/>
                    </Paper>
                  </div>
                  <div className="col-sm-6">
                    <Paper style={exportstyle} zDepth={1}>
                      <h4>Update username</h4>
                          <br/>
                          <TextField type="password" floatingLabelText="Current Password" value={this.state.currpass2} onChange={this.handleCurrentPass2} underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}/>
                          <br/>
                          <TextField type="text" floatingLabelText="New username" value={this.state.newusername} onChange={this.handleNewUsername} underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}/>
                          <br/>
                          <br/>
                          <RaisedButton label="Update" primary={true} onClick={this.handleUpdateUsername}/>
 
                    </Paper>
                  </div>
                </div>
          </div>);
      }

      const actionsbill = [
        <FlatButton
          label="Cancel"
          primary={true}
          onClick={()=>this.setState({openbilling: false})}
        />,
        <FlatButton
          label="OK"
          primary={true}
          onClick={this.handleBilled}
        />,
      ];


    const actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onClick={this.handleCloseAddAdmin}
        disabled={this.state.disableBtnPayOK}
      />,
      <FlatButton
        label="OK"
        primary={true}
        onClick={this.handleInsertPayment}
        disabled={this.state.disableBtnPayOK}
      />,
    ];

    const actions2 = [
      <FlatButton
        label="Cancel"
        primary={true}
        onClick={this.handleCloseRewards}
      />,
      <FlatButton
        label="OK"
        primary={true}
        onClick={this.handleInsertRedeem}
      />,
    ];

    const actions3 = [
      <FlatButton
        disabled = {this.state.progstat}
        label="Cancel"
        primary={true}
        onClick={this.handleCloseAddItem}
      />,
      <FlatButton
        disabled = {this.state.progstat}
        label="OK"
        primary={true}
        onClick={this.handleInsertItem}
      />
    ]

    const AdminLogo = (<div className="divbig" onClick={this.handlePopupMore}>
              <Avatar size={50} className="avatarmenubig">{(this.state.info.length != 0)?this.state.info[0].name.substring(0,1): ""}</Avatar>
              <span className="avatartxtbig">{(this.state.info.length != 0)?this.state.info[0].name: ""}</span>
              <span className="avatarsubtxtbig">{(this.state.info.length != 0)?this.state.info[0].type: ""}</span>
              <Moreexpand className="expandicon" color="white"/>
              <Popover
                open={this.state.openpopup}
                anchorEl={this.state.anchorEl}
                anchorOrigin={{horizontal: 'middle', vertical: 'bottom'}}
                targetOrigin={{horizontal: 'middle', vertical: 'top'}}
                onRequestClose={this.handleClosePopupMore}
                animation={PopoverAnimationVertical}
              >
                <Menu>
                  <MenuItem primaryText="Sign out" onClick={this.handleSignout}/>
                </Menu>
              </Popover>
             </div>);

    const Logged = (
              <IconMenu
                iconButtonElement={
                  <IconButton><MoreVertIcon color="white"/></IconButton>
                }
                targetOrigin={{horizontal: 'right', vertical: 'top'}}
                anchorOrigin={{horizontal: 'right', vertical: 'top'}}
              >

                <div className="menuavatar">
                  <Avatar size={55} className="avatarmenu">{(this.state.info.length != 0)?this.state.info[0].name.substring(0,1): ""}</Avatar>
                  <span className="avatartxt">{(this.state.info.length != 0)?this.state.info[0].name: ""}</span>
                  <span className="avatarsubtxt">{(this.state.info.length != 0)?this.state.info[0].type: ""}</span>
                </div>
                <MenuItem primaryText="Sign out" onClick={this.handleSignout}/>
              </IconMenu>
            );
    
    if(!this.state.session){
       return (<div className="loadParent"><div className="loadChild"><CircularProgress size={this.state.loadersize} thickness={this.state.loaderthick}/></div></div>);
    }
    return(
    <div>
        <AppBar
          className="appshad"
          style={{position: 'fixed'}}
          zDepth={this.state.shadow} 
          title={<div id="titleid" className="titleStyle"><span className="titleFont">{this.state.title}</span></div>}
          onLeftIconButtonClick	= {this.handleToggleDim}
          iconElementLeft={<IconButton><NavigationMenu/></IconButton>}
          iconElementRight={this.state.hideAvater ? Logged : AdminLogo}
        />
        <Drawer open={this.state.opendrawer}>
          <div className="divdrawer">
            <Avatar src={require('./images/Logo.png')} size={40} className="logoavatars" backgroundColor="white" />
            <span className="cotxt" style={{fontSize: '25px'}}>THE COCIRCLE</span>
          </div>
          <MenuItem onClick={this.handlePayments} leftIcon={<Payment />}>Transactions</MenuItem>
          <MenuItem onClick={this.handleItems} leftIcon={<Box />}>Items</MenuItem>
          <MenuItem onClick={this.handlePendingRedemptions} leftIcon={<Redemption />}>Pending Redemptions</MenuItem>
          <MenuItem onClick={this.handleRedeemPoints} leftIcon={<HistoryIcon />}>Redeem History</MenuItem>
          <MenuItem onClick={this.handleCustomers} leftIcon={<Account/>}>Customers</MenuItem>
          <MenuItem onClick={this.handleBilling} leftIcon={<BillIcon />}>Billing Informations</MenuItem>
          <MenuItem onClick={this.handleSecurity} leftIcon={<SecurityIcon />}>Security</MenuItem>
        </Drawer>
        <Drawer
          docked={false}
          open={this.state.opendrawerdim}
          onRequestChange={(opendrawerdim) => this.setState({opendrawerdim})}
        >
          <div className="divdrawer">
            <Avatar src={require('./images/Logo.png')} size={40} className="logoavatars" backgroundColor="white" />
            <span className="cotxt" style={{fontSize: '25px'}}>THE COCIRCLE</span>
          </div>
          <MenuItem onClick={this.handlePayments} leftIcon={<Payment />}>Transactions</MenuItem>
          <MenuItem onClick={this.handleItems} leftIcon={<Box />}>Items</MenuItem>
          <MenuItem onClick={this.handlePendingRedemptions} leftIcon={<Redemption />}>Pending Redemptions</MenuItem>
          <MenuItem onClick={this.handleRedeemPoints} leftIcon={<HistoryIcon />}>Redeem History</MenuItem>
          <MenuItem onClick={this.handleCustomers} leftIcon={<Account/>}>Customers</MenuItem>
          <MenuItem onClick={this.handleBilling} leftIcon={<BillIcon />}>Billing Informations</MenuItem>
          <MenuItem onClick={this.handleSecurity} leftIcon={<SecurityIcon />}>Security</MenuItem>
          
        </Drawer>

        <div key="cont" className="contentContainer">     
          <div className="childcon" key="childconkey">      
               {showhat}
            </div>
        </div>

        

        <Dialog
          style={{overflow: 'hidden'}}
          title="Payment"
          actions={actions}
          modal={true}
          contentStyle={customContentStyle} 
          open={this.state.openaddadmin}
          disabled={true}>
 
          <div className="container">
            <div className="row">
            <div style={{width: '30px'}}>
            <SearchIcon style={{color: '#757575', width: 30, height: 30, 'margin-top': '35px'}}/>
            </div>
            <div className="col" style={{'padding-right': '7px', 'padding-left': '10px'}}>
            <TextField
              fullWidth="true" 
              hintStyle={styles.hintText} 
              underlineStyle={styles.underlineStyle} 
              floatingLabelStyle={styles.labelStyle} 
              floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
              value={this.state.srchcust}
              onChange={this.handleSearchCustomerChange}
              onKeyUp={this.handleKeyDownSrch}
              floatingLabelText="Search member"
              type="text"
              disabled={this.state.disableBtnPayOK}
            />
            </div>
            </div>
          </div>
          <Paper id="dropdown" zDepth={1} style={{width: this.state.paperdiv}} className="paperSrch">
             <List>
               {this.state.customers.map((customers) => (
                  <ListItem primaryText={customers.name} onClick={() => this.hideDropdown(customers.id, customers.name, customers.points)}/>
                ))}
            </List>
          </Paper>

            {/* <div className="container">
            <div className="row">
            <div style={{width: '30px'}}>
            <Payment style={{color: '#757575', width: 30, height: 30, 'margin-top': '35px'}}/>
            </div>
            <div className="col" style={{'padding-right': '7px', 'padding-left': '10px'}}>
            <TextField
              hidden
              fullWidth="true" 
              hintStyle={styles.hintText} 
              underlineStyle={styles.underlineStyle} 
              floatingLabelStyle={styles.labelStyle} 
              floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
              value={this.state.amount} 
              onChange={this.handleAmount}
              // floatingLabelText={"Amount paid in "+this.state.info[0].currency}
              floatingLabelText={"Amount paid"}
              type="number"
              // onKeyUp={this.handleAmountKeyup}
              disabled={this.state.disableBtnPayOK}
            />
            </div>
            </div>
            </div> */}
            <div className="container">
            <div className="row">
            <div style={{width: '30px'}}>
            <Star style={{color: '#757575', width: 30, height: 30, 'margin-top': '35px'}}/>
            </div>
            <div className="col" style={{'padding-right': '7px', 'padding-left': '10px'}}>
            <TextField
              fullWidth="true" 
              hintStyle={styles.hintText} 
              underlineStyle={styles.underlineStyle} 
              floatingLabelStyle={styles.labelStyle} 
              floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
              value={this.state.ptsmember} 
              onChange={this.handlePoints}
              // floatingLabelText={"Amount paid in "+this.state.info[0].currency}
              floatingLabelText={"Points"}
              type="number"
              disabled={this.state.disableBtnPayOK}
            />
            </div>
            </div>
            </div>
            <br/>
            <div hidden style={{'font-weight': '700', 'background-color': '#fafafa', 'padding': '10px', 'border': '0.5px solid #e0e0e0','border-radius': '2px'}}>
                      <span hidden style={{'font-weight': '700'}}>HKD RATE:</span><span hidden style={{color: '#4CAF50', float: 'right'}}>HKD {this.state.currencyRate}</span>
                      {/* <br/> */}
                      <span hidden style={{'font-weight': '700', fontSize: '15px'}}>POINTS:</span><span hidden style={{color: blue500, float: 'right', fontSize: '15px'}}>{this.state.ptsmember} PTS</span>
                      {/* <br/> */}
                      <span hidden style={{'font-weight': '700'}}>TO THE COCIRCLE:</span><span  hidden style={{color: '#4CAF50', float: 'right'}}>$ {this.state.tococircle}</span>                  
                      {/* <br/> */}
                    
           </div>
        </Dialog>

         <Dialog
          style={{overflow: 'hidden'}}
          title="Redeem Points"
          actions={actions2}
          modal={true}
          contentStyle={dialogStyle} 
          open={this.state.opendlreward}>

          <TextField  value={this.state.srchcust} onChange={this.handleSearchCustomerChange} onKeyUp={this.handleKeyDownSrch} floatingLabelText="Search Customer" fullWidth="true" hintStyle={styles.hintText} underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}/>
          <Paper id="dropdown" zDepth={1} style={{width: this.state.paperdiv}} className="paperSrch">
             <List>
               {this.state.customers.map((customers) => (
                  <ListItem primaryText={customers.name} onClick={() => this.hideDropdown(customers.id, customers.name, customers.points)}/>
                ))}
            </List>
          </Paper>
          <TextField type="number" value={this.state.points} floatingLabelText="Points" fullWidth="true" hintStyle={styles.hintText} underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}/>
          <TextField type="number" value={this.state.itemprices} onChange={this.handleItemPriceText} floatingLabelText="Item Price" fullWidth="true" hintStyle={styles.hintText} underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}/>
          <TextField value={this.state.itemnames} onChange={this.handleItemNameText} floatingLabelText="Item Name" fullWidth="true" hintStyle={styles.hintText} underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}/>
        </Dialog>

        <Dialog
          autoScrollBodyContent	= {true}
          autoDetectWindowHeight = {true}
          title="Add Item"
          actions={actions3}
          modal={true}
          contentStyle={customContentStyle2}
          open={this.state.openadditem}>
          {this.state.progstat?<LinearProgress mode="indeterminate" style={{'position':'absolute', 'margin-top':'-76px', 'margin-left':'-24px'}}/>:<div></div>}
          <br/>
          <div className="previewComponent">
            <div className="row">
              <div className="col-sm-6">
              <div>
                {$imagePreview}
              </div>
              </div>
              
              <div className="col-sm-6">
                <div className="row">                
                <div className="col">
                <RaisedButton primary={true} label="Choose file" onClick={this._openFileDialog} disabled = {this.state.progstat}/>
                </div>
                <div className="col">
                <p id="picname" style={{'margin-top':'7px'}}></p>
                </div>
                </div>
                <input 
                ref={input => this.inputElement = input}
                className="fileInput" 
                type="file" 
                onChange={(e)=>this._handleImageChange(e)} 
                style={{"display" : "none"}}
                />

                <TextField
                  disabled = {this.state.progstat}
                  floatingLabelText="Name"
                  value={this.state.itemname}
                  onChange={this.handleChangeItemName}
                  fullWidth="true" hintStyle={styles.hintText} underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                />
                <TextField
                  disabled = {this.state.progstat}
                  floatingLabelText="Price"
                  value={this.state.itemprice}
                  onChange={this.handleChangeItemPrice}
                  fullWidth="true" hintStyle={styles.hintText} underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                />
                <TextField
                  disabled = {this.state.progstat}
                  floatingLabelText="Description"
                  value={this.state.itemdesc}
                  onChange={this.handleChangeItemDesc}
                  fullWidth="true" hintStyle={styles.hintText} underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                />

              </div>
              </div>
          </div>
        </Dialog>
        
        
        <Dialog
          title="Warning"
          actions={actionsbill}
          modal={true}
          open={this.state.openbilling}
        >
          Are you sure this is settled?
        </Dialog>
        
    </div>);
}
  
}

