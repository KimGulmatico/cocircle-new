import React , {Component} from 'react';
import FontIcon from 'material-ui/FontIcon';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import Avatar from 'material-ui/Avatar';
import Checkbox from 'material-ui/Checkbox'
import Fb from 'mui-icons/fontawesome/facebook-official';
import In from 'mui-icons/fontawesome/linkedin-square';
import {blue100, blue500, blue700} from 'material-ui/styles/colors';
import Register from './Register';
import SuperAdminAcc from './SuperAdminAcc';
import axios from 'axios';
import './Login.css';
import Lnk from './LinkedIns';
import Snackbar from 'material-ui/Snackbar';
import Connecton from './Connection'

const server = Connecton.server;
const serverphp = Connecton.serverphp;
const protocol = Connecton.protocol;


const styles = {
  underlineStyle: {
    borderColor: '#9e9e9e',
  },
  labelStyle: {
    color: '#9e9e9e',
  },
  floatingLabelFocusStyle: {
    color: blue500,
  },
};

export default class Login extends Component {

constructor(props) {
  super(props);
  this.state = {
    isRegister: null,
    hasLogin: false,
    user: null,
    pass: null,
    session: false,
    open: false,
    msg: '',
  };
  this.handleLogin = this.handleLogin.bind(this);
}

handleClick = () => {
  this.setState({
    open: true,
  });
};

handleRequestClose = () => {
  this.setState({
    open: false,
  });
};

handleLogin(event){
  this.handlesetStorage();
  //alert(localStorage.getItem('user'));
  axios.get(protocol+'://'+serverphp+'/php/checkUserLogin.php?x='+JSON.stringify(this.state))
    .then(res => {
      var cred = JSON.parse(JSON.stringify(res.data));
      if(cred.length != 0){
        if(cred[0].type == 'SA'){
          this.handlesetStorage();
          window.location.replace(protocol+"://"+server+"/SuperAdmin");
        }
        else if(cred[0].type == 'A'){
          this.handlesetStorage();
          window.location.replace(protocol+"://"+server+"/Admin");
        }
        else if(res.data.trim() == 'C'){
          this.handlesetStorage();
          window.location.replace(protocol+"://"+server+"/User");
        }
        else{
          //alert("Account for customers are not yet available.");
          //alert("Customer account not yet available");
          //alert(res.data);
          this.setState({open: true, msg: res.data});
        }
      }          
  });

}

_handleKeyPress = (e) => {
  if (e.key === 'Enter') {
    this.handleLogin();
  }
}

handlesetStorage(){
  localStorage.setItem('user',this.state.user);
  localStorage.setItem('pass',this.state.pass);
}

componentDidMount() {
  this.handleCheckSession();
}

handleChangeUser = (event) => {
    this.setState({
      user: event.target.value,
    });
};
handleChangePass = (event) => {
    this.setState({
      pass: event.target.value,
    });
};

handleCheckSession(){
  var user = localStorage.getItem('user');
  var pass = localStorage.getItem('pass');
  if(user != "" && pass != ""){
  axios.get(protocol+'://'+serverphp+'/php/checkSessionLogin.php?user='+user+'&pass='+pass)
    .then(res => {
        if(res.data == 'admin'){
          window.location.replace(protocol+'://'+server+'/Admin');
        }
        else if(res.data == 'superadmin'){
          window.location.replace(protocol+'://'+server+'/SuperAdmin');
        }
        else if(res.data == 'C'){
          window.location.replace(protocol+'://'+server+'/User');
        }
        else{
          this.setState({session: true});
        }
  });
  }
  else{
    this.setState({session: true});
  }
}

manualSignup(){
  window.location.replace(protocol+'://'+server+'/Manual');
}


render(){
   
  if(!this.state.session){
    return (<div></div>);
  }
  return(
    <div>
    <Snackbar
          open={this.state.open}
          message={this.state.msg}
          autoHideDuration={4000}
          onRequestClose={this.handleRequestClose}
          bodyStyle={{ color: '#f44336' }}
    />
    <div className="body">
        <div className="back">
          
        </div>
    </div>
    <div className = "logindiv">
      <div className="content">
        <Avatar src={require('./images/Logo.png')} size={100} className="logo" backgroundColor="white"/>
        <div className="childcontent">
          <br/>
          <br/>
          <br/>
          <div className="blockdiv">
          <h2 className="bluetxts roboto">THE COCIRCLE</h2>
          <TextField value={this.state.user} onChange={this.handleChangeUser} floatingLabelText="Email or Username" fullWidth="true" underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}/>
          <br/>
          <TextField value={this.state.pass} onKeyPress={this._handleKeyPress} onChange={this.handleChangePass} floatingLabelText="Password" type="password" fullWidth="true" underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}/>
          <a className="forgotten" onClick={()=> window.location.replace(protocol+"://"+server+'/ForgotPassword')}>Forgotten?</a>
          <br/>
          <br/>
          <br/>
          <div>
          <div className="checkboxfix">
          <div className="divid">
          <Checkbox label="Remember me" className="inline fix"/>
          </div>
          <div className="blk">
          <RaisedButton onClick={this.handleLogin} label="LOGIN" primary={true} className="fix2"/>
          </div>
          </div>
          <div className="divmargins">
          <div align="center">
          </div>
          </div>
          </div>
          <br/>
          <div className="divnew" align="center">
            No account yet? 
            Sign up <span onClick={this.manualSignup} style={{color: '#03A9F4'}}>here~</span> or
            {/* <Lnk/> */}
          </div>
          </div>
        </div>
    </div>
    </div>
    </div>)

}
  
}
