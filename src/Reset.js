import React , {Component} from 'react';
import FontIcon from 'material-ui/FontIcon';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import Avatar from 'material-ui/Avatar';
import Checkbox from 'material-ui/Checkbox'
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import DatePicker from 'material-ui/DatePicker';
import './AccessCode.css';
import {blue100, blue500, blue700} from 'material-ui/styles/colors';
import Login from './Login';
import axios from 'axios';
import Fin from 'mui-icons/evilicons/check';
import Connecton from './Connection'

const server = Connecton.server;
const serverphp = Connecton.serverphp;
const protocol = Connecton.protocol;

// console.log(Connecton.server)

// const server = 'localhost:3000';
// const serverphp = 'localhost:80';


const styles = {
  underlineStyle: {
    borderColor: '#9e9e9e',
  },
  labelStyle: {
    color: '#9e9e9e',
  },
  floatingLabelFocusStyle: {
    color: blue500,
  },
};

export default class AccessCode extends Component {

constructor(props) {
  super(props);
  this.state = {
    pass: null,
    cfrm: null
  };
  this.handleReset = this.handleReset.bind(this);
}

componentDidMount() {

}

handlePass = (event) => {
    this.setState({
      pass: event.target.value,
    });
};

handleCfrm = (event) => {
    this.setState({
      cfrm: event.target.value,
    });
};


handleReset(event) {
    var codeIndex = window.location.toString().indexOf('code');
    var code = window.location.toString().substring(codeIndex).replace('code=', '');

    axios.get(protocol+'://'+serverphp+'/php/resetCustPass.php?pass='+this.state.pass+'&cfrm='+this.state.cfrm+'&code='+code)
        .then(res => {
        alert(res.data)
        if(res.data.trim() == 'Updated')
        window.location.replace(protocol+"://"+server+'/Login')
    });

}


render(){

  return (
      <div>
        <div className="body">
        <div className="back">
          
        </div>
        </div>
        <div className="contmainpp">
          <div className="contchildpp" >
            <Avatar src={require('./images/Logo.png')} size={100} className="logos" backgroundColor="white"/>
            <div className="childcontentpp">
              <br/>
              <br/>
              <br/>
              <br/>
              <div>
                  <div className="blockdivsp">
                    <h4 className="bluetxt" style={{color: blue500}}>RESET PASSWORD</h4>
                    <TextField type="password" value={this.state.pass} onChange={this.handlePass} floatingLabelText="New passwod" fullWidth="true" underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}/>
                    <TextField type="password" value={this.state.cfrm} onChange={this.handleCfrm} floatingLabelText="Confirm password" fullWidth="true" underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}/>
                  </div>
                  <br/>
                  <div >
                    <RaisedButton label="SUBMIT" primary={true} className="btncenter" onClick={this.handleReset}/>
                  </div> 
                  <br/>
                  <div align="center">
                    <a className="havacc" onClick={() => window.location.replace(protocol+"://"+server+'/Login')}>Login</a>
                    <br/>
                    <br/>
                    <br/>
                  </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    );


}

}

