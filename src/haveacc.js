import React , {Component} from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import {blue100, blue500, blue700} from 'material-ui/styles/colors';
import './haveacc.css';
import Connecton from './Connection'

const server = Connecton.server;
const serverphp = Connecton.serverphp;
const protocol = Connecton.protocol;


export default class haveacc extends Component {

constructor(props) {
  super(props);
  this.state = {
    
  };

}

render(){

  return(
    <div className="outer">
    <div className="middle">
        <div className="inner" align="center">

        <h1>You already have an account in our database please login.</h1>
        <br/>
        <RaisedButton onClick={()=> window.location.replace(protocol+'://'+server)} label="LOGIN" primary={true}/>

        </div>
    </div>
    </div>
  );
}
}
