import React , {Component} from 'react';
import In from 'mui-icons/fontawesome/linkedin-square';
import RaisedButton from 'material-ui/RaisedButton';
import Connecton from './Connection'

const server = Connecton.server;
const serverphp = Connecton.serverphp;
const protocol = Connecton.protocol;

export default class LinkedinLogin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isRegister: null,
        };
        this.callbackFunction = this.callbackFunction.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }
    componentDidMount() {
        var liRoot = document.createElement('div');
        liRoot.id = 'linkedin-root';

        document.body.appendChild(liRoot);

        (function(d, s, id) {
            const element = d.getElementsByTagName(s)[0];
            const ljs = element;
            var js = element;
            if (d.getElementById(id)) {
                return; }
            js = d.createElement(s);
            js.id = id;
            js.src = '//platform.linkedin.com/in.js';
            js.text = 'api_key: 815cy3skjhvq85 \n authorize: true \n scope: r_basicprofile r_emailaddress';
            ljs.parentNode.insertBefore(js, ljs);
        }(document, 'script', 'linkedin-sdk'));
    }

    callbackFunction() {
        //window.IN.API.Profile("me").result(function(r) {
            //console.log(r);
            //localStorage.setItem('testObject', JSON.stringify(r));
            //var retrievedObject = localStorage.getItem('testObject');
            //console.log('retrievedObject: ', JSON.parse(retrievedObject));
        //});
        window.IN.API.Profile("me").fields("id","specialties","summary","firstName","lastName","maiden-name","picture-url","headline","publicProfileUrl","location","industry","positions","email-address").result(this.displayProfileData);
    }


    handleClick() {
        //e.preventDefault();
        window.IN.User.authorize(this.callbackFunction, '');
    }
    
    // Use the API call wrapper to request the member's profile data
    getProfileData() {
        window.IN.API.Profile("me").fields("id","specialties","summary","firstName","lastName","maiden-name","picture-url","headline","publicProfileUrl","location","industry","positions","email-address").result(this.displayProfileData);
    }

    displayProfileData(data){
        var user = data.values[0];
        console.log(user);
        localStorage.setItem('testObject', JSON.stringify(user));
        var retrievedObject = localStorage.getItem('testObject');
        window.location.replace(protocol+'://'+server+'/register');
        //console.log('retrievedObject: ', JSON.parse(retrievedObject));
    }


    render() {
        return ( 
            <div>     
            <RaisedButton
            onClick = { this.handleClick }
            backgroundColor="#007bb6"
            labelColor="white"
            target="_blank"
            label="SIGN UP"
            className="btncenter"
            icon={<In color="white" className="fixicon" />}/>
            </div>
        );
    }
}
