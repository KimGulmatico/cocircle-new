import React , {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import FontIcon from 'material-ui/FontIcon';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import Avatar from 'material-ui/Avatar';
import Checkbox from 'material-ui/Checkbox'
import {blue100, blue500, blue700, grey400, darkBlack, lightBlack} from 'material-ui/styles/colors';
import './SuperAdmin.css';
import 'bootstrap/dist/css/bootstrap.css';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import Divider from 'material-ui/Divider';
import DotStat from 'mui-icons/fontawesome/circle';
import Arrow from 'mui-icons/fontawesome/chevron-down';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import NavigationMenu from 'material-ui/svg-icons/navigation/menu';
import ISend from 'material-ui/svg-icons/content/send';
import FlatButton from 'material-ui/FlatButton';
import Popover, {PopoverAnimationVertical} from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import IconMenu from 'material-ui/IconMenu'
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert'
import AdminMan from 'material-ui/svg-icons/action/supervisor-account';
import SecurityIcon from 'material-ui/svg-icons/hardware/security';
import ExportTable from 'material-ui/svg-icons/content/archive';
import UpdateShare from 'material-ui/svg-icons/action/update';
import IInput from 'material-ui/svg-icons/action/input';
import BillIcon from 'material-ui/svg-icons/editor/monetization-on';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import Moreexpand from 'material-ui/svg-icons/navigation/expand-more';
import Check from 'material-ui/svg-icons/action/thumb-up';
import Account from 'material-ui/svg-icons/action/account-box';
import axios from 'axios';
import renderHTML from 'react-render-html';
import Paper from 'material-ui/Paper';
import Dialog from 'material-ui/Dialog';
import Box from 'mui-icons/fontawesome/th-large';
import {List, ListItem} from 'material-ui/List';
import Subheader from 'material-ui/Subheader';
import SelectField from 'material-ui/SelectField';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import CircularProgress from 'material-ui/CircularProgress';
import LinearProgress from 'material-ui/LinearProgress';
import SearchIcon from 'material-ui/svg-icons/action/search';
import ExportBill from 'material-ui/svg-icons/action/play-for-work';
import Snackbar from 'material-ui/Snackbar';
import Redeem from 'material-ui/svg-icons/action/redeem';
import { Collapse} from 'reactstrap';
import Redemption from 'material-ui/svg-icons/action/redeem';
import Payment from 'material-ui/svg-icons/action/payment';
import Connecton from './Connection'

const server = Connecton.server;
const serverphp = Connecton.serverphp;
const protocol = Connecton.protocol;

const styles = {
  underlineStyle: {
    borderColor: '#9e9e9e',
  },
  labelStyle: {
    color: '#9e9e9e',
  },
  floatingLabelFocusStyle: {
    color: blue500,
  },
  hintText:{
    color: '#9e9e9e',
  },
  hideText:{
    color: '#fff',
  },
  smallIcon: {
    width: 30,
    height: 30,
    color: '#757575',
  },
  smallIcons: {
    width: 20,
    height: 20,
    color: '#757575',
  },
  mediumIcon: {
    width: 48,
    height: 48,
    color: '#757575',
  },
  small: {
    width: 50,
    height: 50,
    padding: 0,
  },
  medium: {
    width: 96,
    height: 96,
    padding: 24,
  },
};

const customContentStyle = {
  width: '100%',
  maxWidth: '500px',
};

const customContentStyle2 = {
  width: '100%',
  maxWidth: '650px',
};

const exportstyle = {
  width: '100%',
  padding: '20px'
};

var dateObj = new Date();
var month = dateObj.getUTCMonth() + 1; //months from 1-12
var day = dateObj.getUTCDate();
var year = dateObj.getUTCFullYear();

if(month.toString().length == 1) {
  month = '0'+month;
}

const newdate = year + "-" + month;


export default class SuperAdmin extends Component {

constructor(props) {
  super(props);

  this.state = {
    adminID: 1,	//added by Leo
    cidsend: null,
    paperdiv: '90%',
    paperdivsend: '90%',
    imagefile: '',
    opendrawer: false,
    opendrawerdim: false,
    openpopup: false,
    title: 'Manage Admins',
    hideAvater: false,
    shadow: 1,
    openaddadmin: false,
    acscode: 'Generated code',
    show: 'ManageAdmins',
    session: false,
    openadditem: false,
    user: null,
    pass: null,
    srchcust: null,
    imagePreviewUrl: '',
    itemname: null,
    itemprice: null,
    itemdesc: null,    
    progstat: false,
    info: [],
    infos: [],
    admins: [],
    redeems: [],
    items: [],
    billinfo: [],
    payments: [],
    customers: [],
    loadersize: 40,
    loaderthick: 4,
    collapse: [],
    adminsCpny: [],
    transactCpny: [],
    collapsemanageadmins: [],
    collapseOpen: [],
    reds: [],
    redswithID: [],
    opengift: false,
    srchcustsend: '',
    customerssend: [],
    ptssend: null,
    sendptshistory: [],
    openbilling: false, 
    idtobill: null,
    idinterval1: null,
    idinterval2: null,
    disabledSend: false,
    dateFrom: null,
    dateTo: null,
    dateBill: newdate,
    srchadrs: null,
    admincpny: [],
    admincpnyshare: [],
    cpnyAdminID: null,
    share: 0,
    cpnyIDShare: null, 
    srchadrsshare: null,
    infosShare: [],
    sharePerc: null,
    newpasswd: null,
    cfrmpasswd: null,
    newusername: null,
    currpass1: null,
    currpass2: null,
    currency: null,

  };
  this.handleManageAdmins = this.handleManageAdmins.bind(this);
  this.handleBilling = this.handleBilling.bind(this);
  this.handleClickPopup = this.handleClickPopup.bind(this);
  this.handleRequestClosePopup = this.handleRequestClosePopup.bind(this);
  this.handleCheckSession = this.handleCheckSession.bind(this);
  this.handleLoadRedeem = this.handleLoadRedeem.bind(this);
  this.handleItems = this.handleItems.bind(this);
  this._openFileDialog = this._openFileDialog.bind(this);
  this.handleInsertItem = this.handleInsertItem.bind(this);
  this.handleGetBillingInfo = this.handleGetBillingInfo.bind(this);
  this.handleGetPaymentInfo = this.handleGetPaymentInfo.bind(this);
  this.handleCustomers = this.handleCustomers.bind(this);
  this.handleKeyDownSrch = this.handleKeyDownSrch.bind(this);
  this.srchChange = this.srchChange.bind(this);
  this.handleGetUserInfoProf = this.handleGetUserInfoProf.bind(this);
  this.handleLoadAdminsCpny = this.handleLoadAdminsCpny.bind(this);
  this.handleLoadTransWithID = this.handleLoadTransWithID.bind(this);
  this.toggleMA = this.toggleMA.bind(this);
  this.handleRedemptions = this.handleRedemptions.bind(this);
  this.handleLoadRedeemHistory = this.handleLoadRedeemHistory.bind(this);
  this.handleLoadRedeemHistoryWithID = this.handleLoadRedeemHistoryWithID.bind(this);
  this.handleSendPoints = this.handleSendPoints.bind(this);
  this.handleSearchCustomerChangeSend = this.handleSearchCustomerChangeSend.bind(this);
  this.handleKeyDownSrchSend = this.handleKeyDownSrchSend.bind(this);
  this.handleSendPointsGift = this.handleSendPointsGift.bind(this);
  this.handlePointstoSend = this.handlePointstoSend.bind(this);
  this.handleLoadGiftHistory = this.handleLoadGiftHistory.bind(this);
  this.handleBilled = this.handleBilled.bind(this);
  this.handleOpenBilling = this.handleOpenBilling.bind(this);
  this.handleExport = this.handleExport.bind(this)
  this.handleUpdateShare = this.handleUpdateShare.bind(this)
  this.handleDateFrom = this.handleDateFrom.bind(this)
  this.handleDateTo = this.handleDateTo.bind(this)
  this.srchChangeAdrs = this.srchChangeAdrs.bind(this)
  this.srchChangeAdrsShare = this.srchChangeAdrsShare.bind(this)
  this.handleKeyDownSrchAdrs = this.handleKeyDownSrchAdrs.bind(this)
  this.handleKeyDownSrchAdrsShare = this.handleKeyDownSrchAdrsShare.bind(this)
  this.hideDropdown = this.hideDropdown.bind(this)
  this.hideDropdownSummarry = this.hideDropdownSummarry.bind(this)
  this.hideDropdownShare = this.hideDropdownShare.bind(this)
  this.handleDateBill = this.handleDateBill.bind(this)
  this.handleShare = this.handleShare.bind(this)
  this.handleCreateCode = this.handleCreateCode.bind(this)
  this.handleGetUserInfoProfShare = this.handleGetUserInfoProfShare.bind(this)
  this.handleSharePerc = this.handleSharePerc.bind(this)
  this.updateShare = this.updateShare.bind(this)
  this.handleSecurity = this.handleSecurity.bind(this)
  this.handleNewPasswd = this.handleNewPasswd.bind(this)
  this.handleCfrmPasswd = this.handleCfrmPasswd.bind(this)
  this.handleNewUsername = this.handleNewUsername.bind(this)
  this.handleCurrentPass1 = this.handleCurrentPass1.bind(this)
  this.handleCurrentPass2 = this.handleCurrentPass2.bind(this)
  this.handleUpdatePassword = this.handleUpdatePassword.bind(this)
  this.handleUpdateUsername = this.handleUpdateUsername.bind(this)
  this.handleCurrency = this.handleCurrency.bind(this)
}

handleToggle = () => this.setState({opendrawer: !this.state.opendrawer});

handleToggleDim = () => this.setState({opendrawerdim: !this.state.open});

handleCloseDim = () => this.setState({opendrawerdim: false});

updateDimensions() {
  if(window.innerWidth < 769) {
    this.setState({opendrawer:false});
    this.setState({hideAvater:true});
    this.setState({shadow:2});
    this.setState({paperdiv: document.body.clientWidth});
    this.setState({loadersize: 50, loaderthick: 4,});
  }
  else{
    this.setState({opendrawer:true});
    this.setState({opendrawerdim:false});
    this.setState({hideAvater:false});
    this.setState({shadow:0});
    this.setState({paperdiv: '110%'});
    this.setState({loadersize: 80, loaderthick: 5,});
  } 
}

componentDidMount() {
  this.interval = setInterval(() => this.handleCheckSession(), 1000);
  this.updateDimensions();
  window.addEventListener("resize", this.updateDimensions.bind(this));
  this.handleGetUserInfo();
  this.handleLoadItems();
  this.handleGetBillingInfo();
  this.handleLoadRedeemHistory();
  this.handleLoadGiftHistory();
  this.interval = setInterval(() => this.handleLoadAdmins(), 1000);
  this.interval = setInterval(() => this.handleLoadRedeem(), 1000);
  this.interval = setInterval(() => this.handleGetPaymentInfo(), 1000);
  this.interval = setInterval(() => this.handleLoadAdminsCpny(), 1000);
}

componentWillUnmount() {
  window.removeEventListener("resize", this.updateDimensions.bind(this));
}

handleManageAdmins(){
  this.handleCloseDim();
  this.setState({title: 'Manage Admins'});
  this.setState({maHide: ''});
  this.setState({show: 'ManageAdmins'});
}

handleBilling(){
  this.handleCloseDim();
  this.setState({title: 'Transactions'});
  this.setState({maHide: 'hidden'});
  this.setState({show: 'Transactions'});
}

handleItems(){
  this.handleCloseDim();
  this.setState({title: 'Billing'});
  this.setState({maHide: 'hidden'});
  this.setState({show: 'Billing'});
}

handleCustomers(){
  this.handleCloseDim();
  this.setState({title: 'Customers'});
  this.setState({maHide: 'hidden'});
  this.setState({show: 'Customers'});
}

handleRedemptions(){
  this.handleCloseDim();
  this.setState({title: 'Redemptions'});
  this.setState({maHide: 'hidden'});
  this.setState({show: 'Redemptions'});
}

handleSendPoints(){
  this.handleCloseDim();
  this.setState({title: 'Gift Points'});
  this.setState({maHide: 'hidden'});
  this.setState({show: 'Giftpoints'});
}

handleExport() {
  this.handleCloseDim();
  this.setState({title: 'Export'});
  this.setState({maHide: 'hidden'});
  this.setState({show: 'Export'});
}

handleUpdateShare() {
  this.handleCloseDim();
  this.setState({title: 'Update Share'});
  this.setState({maHide: 'hidden'});
  this.setState({show: 'Share'});
}

handleSecurity() {
  this.handleCloseDim();
  this.setState({title: 'Security'});
  this.setState({maHide: 'hidden'});
  this.setState({show: 'Security'});
}

handleClickPopup(event){
  event.preventDefault();
  this.setState({
      openpopup: true,
      anchorEl: event.currentTarget,
  });
};

handleRequestClosePopup(){
  this.setState({
  openpopup: false,
  });
};

handleOpenAddAdmin = () => {
  //this.handleCreateCode();
  this.setState({openaddadmin: true});
};

handleOpenSendPoints = () => {
  this.setState({opengift: true});
};

handleCloseAddAdmin  = () => {
  this.setState({openaddadmin: false, acscode: null});
};

handleOpenAddItem = () => {
  this.setState({openadditem: true});
};

handleCloseAddItem  = () => {
  this.setState({openadditem: false});
};

handleCreateCode(){
  var randomstring = Math.random().toString(36).slice(-8);
   axios.get(protocol+'://'+serverphp+'/php/creatCode.php?x='+randomstring+'&share='+this.state.share+'&curr='+this.state.currency)
    .then(res => {
      this.setState({acscode: res.data});
  });
}

handleInsertItem(){
  this.setState({progstat: true});
  const data = new FormData();
  data.append('user', this.state.user);
  data.append('pass', this.state.pass);
  data.append('name', this.state.itemname);
  data.append('desc', this.state.itemdesc);
  data.append('price', this.state.itemprice);
  data.append('file', this.state.imagefile);
  axios.post(protocol+'://'+serverphp+'/php/insertItem.php', data)
  .then(res => {
  this.setState({progstat: false, imagePreviewUrl: '', itemdesc: '', itemname: '', itemprice: ''});
  //alert(res.data);
  this.handleLoadItems();
  this.handleCloseAddItem();
  });

}

handleLoadItems(){
  axios.get(protocol+'://'+serverphp+'/php/loadItems.php')
    .then(res => {
     var items = JSON.parse(JSON.stringify(res.data));
     this.setState({items: items});
  });
};

handleLoadAdmins() {
  axios.get(protocol+'://'+serverphp+'/php/render.php')
    .then(res => {
      //document.getElementById("divrender").innerHTML = renderHTML('<RaisedButton label="Default"/>');
     var admins = JSON.parse(JSON.stringify(res.data));
     //var len = this.state.admins.length;
     //admins.slice();
     //alert(admins[1].title);
     if(this.state.admins.length != admins.length)
     this.setState(prevState => ({
        admins: prevState.admins = admins
     }));

     
  });
}

handleLoadRedeem() {
  var user = localStorage.getItem('user');
  var pass = localStorage.getItem('pass');
  axios.get(protocol+'://'+serverphp+'/php/loadRedeemSA.php?user='+user+'&pass='+pass)
    .then(res => {
      //document.getElementById("divrender").innerHTML = renderHTML('<RaisedButton label="Default"/>');
     //const {redeems} = this.state;
     var redeem = JSON.parse(JSON.stringify(res.data));
     //var slc = redeem.slice(0,redeems.length);
     //redeem = redeem.slice(0,this.state.redeems.length);
     //var len = this.state.admins.length;
     //admins.slice();
     //alert(admins[1].title);
     if(this.state.redeems.length != redeem.length)
     this.setState({
        redeems: redeem
     });

     
  });
}

handleNewPasswd = (event) => {
  this.setState({
    newpasswd: event.target.value,
  });
};

handleCfrmPasswd = (event) => {
  this.setState({
    cfrmpasswd: event.target.value,
  });
};

handleNewUsername = (event) => {
  this.setState({
    newusername: event.target.value,
  });
};

handleCurrentPass1 = (event) => {
  this.setState({
    currpass1: event.target.value,
  });
};

handleCurrentPass2 = (event) => {
  this.setState({
    currpass2: event.target.value,
  });
};

handleChangeItemName = (event) => {
  this.setState({
    itemname: event.target.value,
  });
};

handleChangeItemPrice = (event) => {
  this.setState({
    itemprice: event.target.value,
  });
};

handleChangeItemDesc = (event) => {
  this.setState({
    itemdesc: event.target.value,
  });
};

handleCheckSession(){
  var user = localStorage.getItem('user');
  var pass = localStorage.getItem('pass');

  if(this.state.user == null && this.state.pass == null){
    this.setState({user: user});
    this.setState({pass: pass});
  }
  if(user != "" && pass != ""){
  axios.get(protocol+'://'+serverphp+'/php/checkSessionLogin.php?user='+user+'&pass='+pass)
    .then(res => {
        if(res.data == 'admin'){
          window.location.replace(protocol+'://'+server+'/Admin');
        }
        else if(res.data.trim() == 'superadmin'){
          if(this.state.session != true){
          this.setState({session: true});
          }
        }
        else if(res.data.trim() == 'C'){
          window.location.replace(protocol+'://'+server+'/User');
        }
        else{
          window.location.replace(protocol+'://'+server+'/Login');
        }
  });
  }
  else{
    window.location.replace(protocol+'://'+server+'/Login');
  }
}

handleSignout(){
  var user = localStorage.getItem('user');
  var pass = localStorage.getItem('pass');
  axios.get(protocol+'://'+serverphp+'/php/signout.php?user='+user+'&pass='+pass)
    .then(res => {
        localStorage.setItem('user','');
        localStorage.setItem('pass','');
  });
}

handleGetUserInfo(){
  var user = localStorage.getItem('user');
  var pass = localStorage.getItem('pass');
  axios.get(protocol+'://'+serverphp+'/php/getInfoAdmin.php?user='+user+'&pass='+pass)
      .then(res => {
         var info = JSON.parse(JSON.stringify(res.data));
         this.setState({info: info});
      });
}

handleGetBillingInfo(){
  var user = localStorage.getItem('user');
  var pass = localStorage.getItem('pass');
  axios.get(protocol+'://'+serverphp+'/php/getBillingInfo.php?user='+user+'&pass='+pass)
      .then(res => {
         var infos = JSON.parse(JSON.stringify(res.data));
         this.setState({billinfo: infos});
      });
}

handleGetPaymentInfo(){
  var user = localStorage.getItem('user');
  var pass = localStorage.getItem('pass');
  axios.get(protocol+'://'+serverphp+'/php/getPaymentInfo.php?user='+user+'&pass='+pass)
      .then(res => {
         var info = JSON.parse(JSON.stringify(res.data));
         if(info.length != this.state.payments.length){
         this.setState({payments: info});
         }
      });
}

_handleImageChange(e) {
  e.preventDefault();
  
  let reader = new FileReader();
  let file = e.target.files[0];

  reader.onloadend = () => {
    this.setState({
      imagefile: file,
      imagePreviewUrl: reader.result
    });
  }
  document.getElementById('picname').innerHTML = e.target.files[0].name;
  reader.readAsDataURL(file)
}

_openFileDialog(){
  this.inputElement.click();
}

//Searching
srchChange(event){
  this.setState({srchcust: event.target.value});
}

handleKeyDownSrch(event){
  axios.get(protocol+'://'+serverphp+'/php/searchCust.php?x='+event.target.value+'&adminID='+this.state.adminID)
      .then(res => {
        var customers = JSON.parse(JSON.stringify(res.data));
        this.setState({customers: customers}); 
        if(this.state.customers.length !== 0){
          document.getElementById("dropdown").style.display = "block";
        }else{
          document.getElementById("dropdown").style.display = "none";
        }
      });
}

hideDropdown(id, name){
  this.setState({custID: id, srchcust: name});
  document.getElementById("dropdown").style.display = "none";
  this.handleGetUserInfoProf(id);
}

handleGetUserInfoProf(id){
  axios.get(protocol+'://'+serverphp+'/php/getUserInfoIDSrch.php?id='+id)
      .then(res => {
         var info = JSON.parse(JSON.stringify(res.data));
         this.setState({infos: info});
      });
}

hideDropdownShare(id, name){
  //alert(points);
  this.setState({cpnyIDShare: id, srchadrsshare: name});
  document.getElementById("dropdownShare").style.display = "none";
  this.handleGetUserInfoProf(id);
}

handleGetUserInfoProfShare(id){
  axios.get(protocol+'://'+serverphp+'/php/getUserInfoIDSrch.php?id='+id)
      .then(res => {
         var info = JSON.parse(JSON.stringify(res.data));
         this.setState({infosShare: info});
      });
}

toggle(index){
  clearInterval(this.state.idinterval1);
  clearInterval(this.state.idinterval2);
  this.setState({transactCpny: [], redswithID: []});
  var arrayvar = this.state.collapse.slice()
  var lastOpen = this.state.collapseOpen;
  arrayvar[lastOpen] = false;
  if(lastOpen != index){
    arrayvar[index] = !arrayvar[index];
  }
  if(lastOpen == index){
    this.setState({collapseOpen: null});
  }
  else{
    this.setState({collapseOpen: index});
  }
 
  this.setState({ collapse: arrayvar })
  this.handleLoadTransWithID(index);
  this.handleLoadRedeemHistoryWithID(index);
  var interval1 = setInterval(() => this.handleLoadTransWithID(index), 1000);
  var interval2 = setInterval(() => this.handleLoadRedeemHistoryWithID(index), 1000);
  
  this.setState({idinterval1:  interval1});
  this.setState({idinterval2:  interval2});
}


toggleMA(index){
  var arrayvar = this.state.collapsemanageadmins.slice()
  arrayvar[index] = !arrayvar[index];
  this.setState({ collapsemanageadmins: arrayvar })
}


handleLoadAdminsCpny() {
  axios.get(protocol+'://'+serverphp+'/php/loadAdminCpny.php')
    .then(res => {
     var admins = JSON.parse(JSON.stringify(res.data));
     this.setState({adminsCpny: admins});
     
  });
}

handleLoadTransWithID(id) {
  axios.get(protocol+'://'+serverphp+'/php/loadTransactionWithID.php?adminID='+id)
    .then(res => {
     var trans = JSON.parse(JSON.stringify(res.data));
     this.setState({transactCpny: trans});
  });
}

handleBilled() {
  this.setState({disabledBillBtn:true})
  axios.get(protocol+'://'+serverphp+'/php/updateBilling.php?adminID='+this.state.idtobill)
    .then(res => {
     alert(res.data);
     this.handleLoadAdminsCpny();
     this.handleLoadRedeemHistoryWithID(this.state.idtobill);
     this.handleLoadTransWithID(this.state.idtobillid);
     this.setState({openbilling: false});
     this.setState({disabledBillBtn: false})
  });
}

handleOpenBilling(id){
  this.setState({openbilling: true, idtobill: id});
}

handleLoadRedeemHistory() {
  var user = localStorage.getItem('user');
  var pass = localStorage.getItem('pass');
  axios.get(protocol+'://'+serverphp+'/php/loadRedeemHistorySA.php')
    .then(res => {
    var rdhst = JSON.parse(JSON.stringify(res.data));
    this.setState({reds: rdhst});
  });
}

handleLoadRedeemHistoryWithID(id) {
  axios.get(protocol+'://'+serverphp+'/php/loadRedeemHistorySAWithID.php?adminID='+id)
    .then(res => {
    var rdhst = JSON.parse(JSON.stringify(res.data));
    this.setState({redswithID: rdhst});
  });

}

//Searching for send

handleSearchCustomerChangeSend(event){
  this.setState({srchcustsend: event.target.value});
}

handleKeyDownSrchSend(event){
  axios.get(protocol+'://'+serverphp+'/php/searchCust.php?x='+event.target.value)
      .then(res => {
        var customers = JSON.parse(JSON.stringify(res.data));
        this.setState({customerssend: customers}); 
        if(this.state.customerssend.length !== 0){
          document.getElementById("dropdowngift").style.display = "block";
        }else{
          document.getElementById("dropdowngift").style.display = "none";
        }
      });
}

hideDropdownSend(id, name){
  
  this.setState({srchcustsend: name});
  this.setState({cidsend: id});
  document.getElementById("dropdowngift").style.display = "none";

}

handleSendPointsGift(){
  var user = localStorage.getItem('user');
  var pass = localStorage.getItem('pass');
  this.setState({disabledSend: true})
  axios.get(protocol+'://'+serverphp+'/php/sendPointsToMember.php?cID='+this.state.cidsend+'&points='+this.state.ptssend+'&user='+user+'&pass='+pass)
    .then(res => {
      this.setState({opengift: false, ptssend: null, srchcustsend: '', disabledSend: false});
      this.handleLoadGiftHistory();
  });
}

handlePointstoSend(event){
  this.setState({ptssend: event.target.value});
}

handleLoadGiftHistory(){
  axios.get(protocol+'://'+serverphp+'/php/loadGiftPointsHistory.php')
    .then(res => {
      var hist = JSON.parse(JSON.stringify(res.data));
      if(hist.length != this.state.sendptshistory.length)
      this.setState({sendptshistory: hist});
  });
}

handleDateFrom(event){
  this.setState({dateFrom: event.target.value});
}

handleShare(event){
  this.setState({share: event.target.value});
}

handleCurrency(event, index, value){
  this.setState({currency: value});
}

handleDateTo(event){
  this.setState({dateTo: event.target.value});
}

handleDateBill(event){
  this.setState({dateBill: event.target.value});
}

srchChangeAdrs(event){
  this.setState({srchadrs: event.target.value});
}

srchChangeAdrsShare(event){
  this.setState({srchadrsshare: event.target.value});
}

handleKeyDownSrchAdrs(event){
  axios.get(protocol+'://'+serverphp+'/php/searchCoworking.php?x='+event.target.value)
      .then(res => {
        var cpny = JSON.parse(JSON.stringify(res.data));
        this.setState({admincpny: cpny}); 
        if(this.state.admincpny.length !== 0){
          document.getElementById("dropdown").style.display = "block";
        }else{
          document.getElementById("dropdown").style.display = "none";
        }
      });
}

handleKeyDownSrchAdrsShare(event){
  axios.get(protocol+'://'+serverphp+'/php/searchCoworking.php?x='+event.target.value)
      .then(res => {
        var cpny = JSON.parse(JSON.stringify(res.data));
        this.setState({admincpnyshare: cpny}); 
        if(this.state.admincpnyshare.length !== 0){
          document.getElementById("dropdownShare").style.display = "block";
        }else{
          document.getElementById("dropdownShare").style.display = "none";
        }
      });
}

hideDropdownSummarry(id, name){
  this.setState({cpnyAdminID: id, srchadrs: name});
  document.getElementById("dropdown").style.display = "none";
  //this.handleLoadItems(id);
}

handleSharePerc(event) {
  if(event.target.value.toString().length <= 2)
  this.setState({sharePerc: event.target.value})
}

updateShare() {
  axios.get(protocol+'://'+serverphp+'/php/updateShare.php?id='+this.state.cpnyIDShare+'&share='+this.state.sharePerc)
      .then(res => {
        alert(res.data)
      });
}

handleUpdatePassword(){
  
  axios.post(protocol+'://'+serverphp+'/php/updatePassword.php?user='+this.state.user+'&pass='+this.state.newpasswd+'&cfrmpass='+this.state.cfrmpasswd+'&oldpass='+this.state.currpass1)
  .then(res => {
    if(res.data.trim() == "changed"){
      localStorage.setItem('pass',this.state.newpasswd);
      this.setState({pass: this.state.newpasswd,
                     newpasswd: '',
                     cfrmpasswd: '',
                     currpass1: ''})
      alert('Updated');
    }
    else {
      alert(res.data);
    }
  });

}

handleUpdateUsername(){
  
  axios.post(protocol+'://'+serverphp+'/php/updateUsername.php?user='+this.state.user+'&newuser='+this.state.newusername+'&oldpass='+this.state.currpass2)
  .then(res => {
    if(res.data.trim() == "changed"){
      localStorage.setItem('user',this.state.newusername);
      this.setState({user: this.state.newusername,
                     newusername: '',
                     currpass2: ''})
      alert('Updated');
    }
    else {
      alert(res.data);
    }
  });

}

render(){
    let {imagePreviewUrl} = this.state;
    let $imagePreview = null;
    if (imagePreviewUrl) {
      $imagePreview = (<div><img src={imagePreviewUrl} width="100%" height="255px" /></div>);
    } else {
      $imagePreview = (<div><img src={require('./images/addimage.png')} width="100%" height="255px" /></div>);
    }
    let {states} = this.state;
    var showhat;
      if(this.state.show == "ManageAdmins"){
        showhat = (
        <div  style={{height: '100%'}}>   
          <Paper zDepth={1}>        
              <List>
                      {(this.state.admins.length != 0)?this.state.admins.map((ad) => (
                      <div>
                      <div>
                      <ListItem
                        primaryText={ad.name + ' (' + ad.user + ') '}
                        leftAvatar={<Avatar>{ad.name.substring(0,1)}</Avatar>}
                        rightIcon={ <Moreexpand/>}
                        onClick={()=>this.toggleMA(ad.id)}  
                      />
                      <Divider inset={true} />
                      </div>
                      <Collapse isOpen={this.state.collapsemanageadmins[ad.id]}>
                      <Divider/>
                      <div className="container">
                      <div className="row">
                      <div className="col-5">
                        <div className="centered">
                            <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Birthdate:</p>                          
                        </div>
                      </div>
                      <div className="col-7">
                      <TextField hintText="Birtdate" underlineShow={false}  fullWidth={true} value={ad.bday}/>
                      </div>
                      </div>
                      <div className="row">
                      <div className="col-5">
                        <div className="centered">
                            <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Mobile:</p>                          
                        </div>
                      </div>
                      <div className="col-7">
                      <TextField hintText="Birtdate" underlineShow={false}  fullWidth={true} value={ad.mob}/>
                      </div>
                      </div>
                      <div className="row">
                      <div className="col-5">
                        <div className="centered">
                            <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Company:</p>                          
                        </div>
                      </div>
                      <div className="col-7">
                      <TextField hintText="Birtdate" underlineShow={false}  fullWidth={true} value={ad.cpnyname}/>
                      </div>
                      </div>
                      <div className="row">
                      <div className="col-5">
                        <div className="centered">
                            <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Address:</p>                          
                        </div>
                      </div>
                      <div className="col-7">
                      <TextField hintText="Birtdate" underlineShow={false}  fullWidth={true} value={ad.cpnyadrs}/>
                      </div>
                      </div>
                      <div className="row">
                      <div className="col-5">
                        <div className="centered">
                            <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Buisness ID:</p>                          
                        </div>
                      </div>
                      <div className="col-7">
                      <TextField hintText="Birtdate" underlineShow={false}  fullWidth={true} value={ad.cpnyid}/>
                      </div>
                      </div>
                      <div className="row">
                      <div className="col-5">
                        <div className="centered">
                            <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Company #:</p>                          
                        </div>
                      </div>
                      <div className="col-7">
                      <TextField hintText="Birtdate" underlineShow={false}  fullWidth={true} value={ad.cpnycon}/>
                      </div>
                      </div>
                      </div>
                      <Divider/>
                      </Collapse>
                      </div>
                      )):<p style={{padding: '15px'}}>Nothing to see.</p>}
              </List>
        
        <FloatingActionButton className="Floatingbtn" onClick={this.handleOpenAddAdmin }>
          <ContentAdd />
        </FloatingActionButton>
        </Paper>
        </div>);
      }
      else if(this.state.show == "Transactions"){
         const {billinfo} = this.state;
        showhat = (
                <div>
                <div className="container">
                  <div className="row">
                    <div className="col-sm-4">
                      <Paper z-zDepth={1} style={{padding: '20px'}}>
                        <span style={{color: lightBlack}}>Total Points</span><br/>
                        <span style={{'font-size':'30px', color: blue500}}>{(billinfo[0].total != null )?billinfo[0].total:'0'} pts</span>
                      </Paper>
                    </div>  
                    <div className="col-sm-4" >
                      <Paper z-zDepth={1} style={{padding: '20px'}}>
                        <span style={{color: lightBlack}}>Avail Points</span><br/>
                        <span style={{'font-size':'30px', color: blue500}}>{(billinfo[0].avail != null )?billinfo[0].avail:'0'} pts</span>
                      </Paper>
                    </div>  
                    <div className="col-sm-4">
                      <Paper z-zDepth={1} style={{padding: '20px'}}>
                        <span style={{color: lightBlack}}>Expired Points</span><br/>
                        <span style={{'font-size':'30px', color: blue500}}>{(billinfo[0].expired != null )?billinfo[0].expired:'0'} pts</span>
                      </Paper>
                    </div>  
                  </div>
                </div>
                <br/>
                <Paper  zDepth={1} style={{height: '100%'}}>
                  <div>
                    <Subheader style={{'font-weight': '700', 'background-color': '#fafafa'}}>TRANSACTIONS</Subheader>
                    <Divider/>
                    <List>
                    {(this.state.payments.length != 0)?this.state.payments.map((rd) => (
                    <div>
                    <ListItem
                      leftAvatar={<Avatar src={rd.cpic} />}
                      primaryText={rd.cname+'(Customer)'}
                      secondaryText={
                        <p>
                          <span style={{color: darkBlack}}>{rd.aname+'(Operator)'}</span><br/>
                          {rd.desc.replace(/[\u00A0-\u9999<>\&]/gim, function(i) {
                              return '&#'+i.charCodeAt(0)+';';
                            })}
                        </p>
                      }
                      secondaryTextLines={2}
                    />
                    <Divider inset={true} />
                    </div>
                    )):<p style={{padding: '15px'}}>Nothing to see.</p>}
                    </List>
                   
                  </div>
                </Paper> 
                </div>);
      }
      else if(this.state.show == "Billing"){
        const {billinfo} = this.state;
        showhat = (<div>
                     {(this.state.adminsCpny.length != 0)?this.state.adminsCpny.map((ad) => (        
                    <Paper zDepth={1} style={{height: 'auto'}}>
                      <div>
                      <div className="container">
                      <div className="row">
                            <div className="col">
                            <p style={{'font-weight': '700', 'font-size': '15px', color: '#757575', 'margin-top':'13px'}}>{ad.cpnyname}</p>
                            </div>
                            <div style={{width: '50px', float: 'right'}}>                                                
                            </div>
                            <div style={{width: '50px', float: 'right'}}>                                                
                            <IconButton hidden={(ad.stat == "neg" || ad.stat == "zero")?true:false} onClick={()=>this.handleOpenBilling(ad.id)} tooltipPosition="bottom-right" style={styles.small} iconStyle={styles.smallIcons}>
                              <Check/>
                            </IconButton> 
                            </div>
                            <div style={{width: '50px', 'float': 'right'}}>  
                            <IconButton onClick={()=>{window.open(protocol+'://'+serverphp+'/php/exportBilling2.php?id='+ad.id+'&cpny='+ad.cpnyname, '_blank')}} tooltipPosition="bottom-right" style={styles.small} iconStyle={styles.smallIcon}>
                              <ExportBill/>
                            </IconButton>
                            </div>
                            <div style={{width: '50px', 'float': 'right'}}>  
                            <IconButton onClick={()=>this.toggle(ad.id)} tooltipPosition="bottom-right" style={styles.small} iconStyle={styles.smallIcon}>
                              <Moreexpand/>
                            </IconButton>
                            </div>
                      </div>
                      <div className="row">
                            <div className="col">
                            <p style={{'font-size': '13px', 'margin-top': '-14px'}}>{ad.cpnyadrs}</p>
                            <p style={{'font-size': '13px', 'margin-top': '-14px'}}>{ad.name}(Operator)</p>
                            </div>
                      </div>
                      <div className="row">
                            <div className="col">
                            <p style={{'font-weight': '700', 'font-size': '15px', color: '#757575'}}>Total Points Issued:</p>
                            </div>
                            <div style={{width: '100px'}}>
                            <p style={{'font-weight': '700', float: 'right', color: blue500, 'margin-right': '18px', 'font-size': '15px'}}>{ad.tpi} pts</p>
                            </div>
                      </div>
                      <div className="row">
                            <div className="col">
                            <p style={{'font-weight': '700', 'font-size': '15px', color: '#757575'}}>Total Points Redeemed:</p>
                            </div>
                            <div style={{width: '100px'}}>
                            <p style={{'font-weight': '700', float: 'right', 'margin-right': '18px', 'font-size': '15px', color: '#F44336'}}>{ad.tps} pts</p>
                            </div>
                      </div>
                      <div className="row">
                            <div className="col">
                            <p style={{'font-weight': '700', 'font-size': '15px', color: '#757575'}}>Share by Admin:</p>
                            </div>
                            <div style={{width: '100px'}}>
                            <p style={{'font-weight': '700', float: 'right', 'margin-right': '18px', 'font-size': '15px', color: '#F44336'}}>{ad.share} %</p>
                            </div>
                      </div>
                      <div className="row">
                            <div className="col">
                            <p style={{'font-weight': '700', 'font-size': '15px', color: '#757575'}}>Share by The Cocircle:</p>
                            </div>
                            <div style={{width: '100px'}}>
                            <p style={{'font-weight': '700', float: 'right', 'margin-right': '18px', 'font-size': '15px', color: '#F44336'}}>{ad.ccshare} %</p>
                            </div>
                      </div>
                      <div className="row">
                            <div className="col">
                            <p style={{'font-weight': '700', 'font-size': '15px', color: '#757575'}}>Total:</p>
                            </div>
                            <div style={{width: '250px'}}>
                            <p style={{'font-weight': '700', float: 'right', 'margin-right': '18px', 'font-size': '15px', color: '#4CAF50'}}>{Math.abs(ad.totalpts)} pts {(ad.stat == "neg")?"to":""} {(ad.stat == "zero")?"":""} {(ad.stat == "pos")?"from":""} {(ad.stat != "zero")?"Company":""} </p>
                            </div>
                      </div>
                      </div>
                      <Collapse isOpen={this.state.collapse[ad.id]}>
                      <Divider />
                      <Subheader style={{'font-weight': '700', 'background-color': '#fafafa'}}>POINTS ISSUED</Subheader>
                      <Divider />
                      <List>
                      {(this.state.transactCpny.length != 0)?this.state.transactCpny.map((rd) => (
                      <div>
                      <ListItem
                        leftAvatar={<Avatar src={rd.cpic} />}
                        primaryText={rd.cname+'(Customer)'}
                        secondaryText={
                          <p>
                            <span style={{color: darkBlack}}>{rd.aname+'(Operator)'}</span><br/>
                            {rd.desc.replace(/[\u00A0-\u9999<>\&]/gim, function(i) {
                                return '&#'+i.charCodeAt(0)+';';
                              })}
                          </p>
                        }
                        secondaryTextLines={2}
                      />
                      <Divider inset={true} />
                      </div>
                      )):<p style={{padding: '15px'}}>Nothing to see.</p>}
                      </List>
                      <Divider />
                      <Subheader style={{'font-weight': '700', 'background-color': '#fafafa'}}>REDEMPTIONS</Subheader>
                      <Divider />
                      <List key="MainlistRedeem">
                      {(this.state.redswithID.length != 0)?this.state.redswithID.map((rd) => (
                      <div>
                      <ListItem
                        key={'lirhistory'+rd.id}
                        leftAvatar={<Avatar key={'lirhistoryavatar'+rd.id} src={rd.pic} />}
                        primaryText={rd.cname+'(Customer)'}
                        secondaryText={
                          <p key={'lirhistorydesc2'+rd.id}>
                            <span key={'lirhistorydesc'+rd.id} style={{color: darkBlack}}>{rd.desc}</span><br/>
                            {rd.desc2}
                          </p>
                        }
                        secondaryTextLines={2}
                      />
                      <Divider inset={true} />
                      </div>
                      )):<p style={{padding: '15px'}}>Nothing to see.</p>}
                      </List>
                      </Collapse>
                    </div>
                  </Paper> 
                  )):<p style={{padding: '15px'}}>Nothing to see.</p>}
                  </div>);
      }
       else if(this.state.show == "Customers"){
        const {customers} = this.state;
        showhat = (<div style={{width: '100%', 'min-height': '100vh', overflow: 'hidden'}}>
              <Paper zDepth={1}  style={{padding: '0px 10px 0px 10px', 'max-width': '400px', 'margin-left': 'auto', 'margin-right': 'auto'}} >
                        <div>
                        <div className="row">
                        <div className="col-sm-12">   
                          <div className="row">    
                          <div className="col">     
                          <TextField style={{'font-size': '20px'}} hintText="Search Customer" underlineShow={false} value={this.state.srchcust} fullWidth={true} hintStyle={styles.hintText} onChange={this.srchChange} onKeyUp={this.handleKeyDownSrch}/>
                          <Paper align="left" id="dropdown" zDepth={1} style={{width: this.state.paperdiv, 'max-width': '400px','margin': '0 -10px 0 -10px'}} className="dropdownsrch">
                            <List>
                              {this.state.customers.map((customers) => (
                                  <ListItem primaryText={customers.name} onClick={() => this.hideDropdown(customers.id, customers.name)}/>
                                ))}
                            </List>
                          </Paper>
                          </div>
                          <div className="col-1">
                          <SearchIcon style={{width: 30, height: 30, color: '#9e9e9e', 'margin-top':'10px','position':'absolute', right: '10px'}}/>
                          </div>
                          </div>
                        </div>
                        </div>
                        </div>
                </Paper>
                <div>
                {this.state.infos.length != 0?
                      <div style={{padding: '7.5px'}}>
                <Paper zDepth={1} className="paperCon" align="center" style={{padding: '20px'}}>
                  <Avatar src={this.state.infos[0].lnkPicUrl} size={125}/>
                  <br/>
                  <br/>
                  <h4 className="primarytxt">{this.state.infos[0].fname+' '+this.state.infos[0].lname}</h4>
                </Paper>
                <Paper zDepth={1} className="paperCon" style={{overflow:'hidden'}}>
                  <Subheader style={{'font-weight': '700', 'background-color': '#fafafa'}}>PERSONAL INFO</Subheader>
                  <Divider />
                  <div className="container">
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Firstname:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField hintText="Firstname" underlineShow={false}  fullWidth={true} value={this.state.infos[0].fname}/>
                  </div>
                  </div>
                   <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Lastname:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField hintText="Lastname" underlineShow={false} fullWidth={true} value={this.state.infos[0].lname}/>
                  </div>
                  </div>
                  <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Birthday:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField hintText="Birthday" underlineShow={false}  fullWidth={true} value={this.state.infos[0].dob}/>
                  </div>
                  </div>
                  <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Email:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField hintText="Email" underlineShow={false}  fullWidth={true} value={this.state.infos[0].email}/>
                  </div>
                  </div>
                  <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Mobile:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField hintText="Mobile" underlineShow={false}  fullWidth={true} value={this.state.infos[0].mobile}/>
                  </div>
                  </div>
                  </div>
                </Paper>

                <Paper zDepth={1} className="paperCon" style={{overflow:'hidden'}}>
                  <Subheader style={{'font-weight': '700', 'background-color': '#fafafa'}}>LINKEDIN INFO</Subheader>
                  <Divider />
                  <div className="container">
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Headline:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField hintText="Headline" underlineShow={false}  fullWidth={true} value={this.state.infos[0].lnkHeadline}/>
                  </div>
                  </div>
                   <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Profile URL:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField hintText="Profile URL" underlineShow={false} fullWidth={true} value={this.state.infos[0].lnkProfileUrl}/>
                  </div>
                  </div>
                  <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Location:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField hintText="Location" underlineShow={false}  fullWidth={true} value={this.state.infos[0].lnkLocation}/>
                  </div>
                  </div>
                  <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Summary:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField hintText="Summary" underlineShow={false}  fullWidth={true} value={this.state.infos[0].lnkSummary}/>
                  </div>
                  </div>
                  <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Specialties:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField hintText="Specialties" underlineShow={false}  fullWidth={true} value={this.state.infos[0].lnkSpecialties}/>
                  </div>
                  </div>
                  </div>
                </Paper>

                <Paper zDepth={1} className="paperCon" style={{overflow:'hidden'}}>
                  <Subheader style={{'font-weight': '700', 'background-color': '#fafafa'}}>COMPANY INFO</Subheader>
                  <Divider />
                  <div className="container">
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Name:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField hintText="Name" underlineShow={false}  fullWidth={true} value={this.state.infos[0].cpnyName}/>
                  </div>
                  </div>
                   <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Sector:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField hintText="Sector" underlineShow={false} fullWidth={true} value={this.state.infos[0].cpnySector}/>
                  </div>
                  </div>
                  <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Size:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField hintText="Size" underlineShow={false}  fullWidth={true} value={this.state.infos[0].cpnySize}/>
                  </div>
                  </div>
                  <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Start date:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField hintText="Start date" underlineShow={false}  fullWidth={true} value={this.state.infos[0].cpnyStartDate}/>
                  </div>
                  </div>
                  <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Summary:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField hintText="Summary" underlineShow={false}  fullWidth={true} value={this.state.infos[0].cpnySummary}/>
                  </div>
                  </div>
                  <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Location:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField hintText="Location" underlineShow={false}  fullWidth={true} value={this.state.infos[0].cpnyLocation}/>
                  </div>
                  </div>
                  <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Contact:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField hintText="Contact" underlineShow={false}  fullWidth={true} value={this.state.infos[0].cpnyContact}/>
                  </div>
                  </div>
                  <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Website:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField hintText="Website" underlineShow={false}  fullWidth={true} value={this.state.infos[0].cpnyWeb}/>
                  </div>
                  </div>
                  <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Title:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField hintText="Summary" underlineShow={false}  fullWidth={true} value={this.state.infos[0].cpnyTitle}/>
                  </div>
                  </div>
                  <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Description:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField hintText="Description" underlineShow={false}  fullWidth={true} value={this.state.infos[0].cpnyDesc}/>
                  </div>
                  </div>
                  <Divider/>
                  <div className="row">
                  <div className="col-4">
                    <div className="centered">
                        <p style={{'line-height': '1.5rem', 'margin':'0', 'padding': '0'}} className="primarytxt">Logo URL:</p>                          
                    </div>
                  </div>
                  <div className="col-8">
                  <TextField hintText="Logo URL" underlineShow={false}  fullWidth={true} value={this.state.infos[0].cpnyLogo}/>
                  </div>
                  </div>
                  </div>
                </Paper>

                </div>
                :''}
                  </div>

                </div>);
      }
      else if(this.state.show == "Redemptions"){
        showhat = (
          <div key="mainRedeemDiv" style={{height: '100%'}}>
                <Paper zDepth={1} style={{height: '100%'}}>
                    <div>
                      <List key="MainlistRedeem">
                      {(this.state.reds.length != 0)?this.state.reds.map((rd) => (
                      <div>
                      <ListItem
                        key={'lirhistory'+rd.id}
                        leftAvatar={<Avatar key={'lirhistoryavatar'+rd.id} src={rd.pic} />}
                        primaryText={rd.cname}
                        secondaryText={
                          <p key={'lirhistorydesc2'+rd.id}>
                            <span key={'lirhistorydesc'+rd.id} style={{color: darkBlack}}>{rd.desc}</span><br/>
                            {rd.desc2}
                          </p>
                        }
                        secondaryTextLines={2}
                      />
                      <Divider inset={true} />
                      </div>
                      )):<p style={{padding: '15px'}}>Nothing to see.</p>}
                      </List>
                     
                    </div>
                  </Paper>  
          </div>);
      }
      else if(this.state.show == "Giftpoints"){
        showhat = (
          <div style={{height: '100%'}}>
                <Paper zDepth={1} style={{height: '100%'}}>
                    <div>
                      <List >
                      {(this.state.sendptshistory.length != 0)?this.state.sendptshistory.map((rd) => (
                      <div>
                      <ListItem
                        leftAvatar={<Avatar src={rd.url} />}
                        primaryText={rd.name}
                        secondaryText={
                          <p>
                            <span style={{color: darkBlack}}>{rd.desc}</span><br/>
                            {rd.desc2}
                          </p>
                        }
                        secondaryTextLines={2}
                      />
                      <Divider inset={true} />
                      </div>
                      )):<p style={{padding: '15px'}}>Nothing to see.</p>}
                      </List>
                     
                    </div>
                  </Paper>  
        <FloatingActionButton className="Floatingbtn" onClick={this.handleOpenSendPoints }>
          <ISend />
        </FloatingActionButton>
          </div>);
      }
      else if(this.state.show == "Export"){
        showhat = (
          <div style={{height: '100%'}}>
                <div className="row">
                  <div className="col-sm-6">
                    <Paper style={exportstyle} zDepth={1}>
                      <h4>Points Expiry</h4>
                      <br/>
                      <TextField type="date" floatingLabelFixed="true" floatingLabelText="Date From" value={this.state.dateFrom} onChange={this.handleDateFrom} underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}/>
                      <br/>
                      <TextField type="date" floatingLabelFixed="true" floatingLabelText="Date To" value={this.state.dateTo} onChange={this.handleDateTo} underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}/>
                      <br/>
                      <br/>
                      <RaisedButton label="Download CSV" primary={true} onClick={()=> { window.open(protocol+'://'+serverphp+'/php/csv.php?dateFrom='+this.state.dateFrom+'&dateTo='+this.state.dateTo, '_blank');}}/>
                    </Paper>
                  </div>
                  <div className="col-sm-6">
                    <Paper style={exportstyle} zDepth={1}>
                      <h4>Monthly point summary</h4>
                          <br/>
                          <TextField style={{'font-size': '20px', width: '51%'}} floatingLabelText="Search coworking space" underlineShow={true} value={this.state.srchadrs} hintStyle={styles.hintText} onChange={this.srchChangeAdrs} onKeyUp={this.handleKeyDownSrchAdrs} underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}/>
                          <Paper align="left" id="dropdown" zDepth={1} style={{width: '50.5%', 'max-width': '50.5% !important','margin': '0 -10px 0 -10px'}} className="dropdownsrch">
                            <List>
                            {this.state.admincpny.map((company) => (
                                  <ListItem primaryText={company.name} onClick={() => this.hideDropdownSummarry(company.id, company.name)}/>
                                ))}
                            </List>
                          </Paper>
                          <br/>
                          <TextField type="month" floatingLabelFixed="true" floatingLabelText="Month/Year" value={this.state.dateBill} onChange={this.handleDateBill} underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}/>
                          <br/>
                          <br/>
                          <RaisedButton label="Download CSV" primary={true} onClick={()=> { window.open(protocol+'://'+serverphp+'/php/exportBilling.php?id='+this.state.cpnyAdminID+'&date='+this.state.dateBill, '_blank');}}/>
 
                    </Paper>
                  </div>
                  <div className="col-sm-6" style={{marginTop: '28px'}}>
                    <Paper style={exportstyle} zDepth={1}>
                      <h4>Full Report of Customers</h4>
                      <br/>
                      <RaisedButton label="Download CSV" primary={true} onClick={()=> { window.open(protocol+'://'+serverphp+'/php/exportCustomers.php?dateFrom', '_blank');}}/>
                    </Paper>
                  </div>
                </div>
          </div>);
      }
      else if(this.state.show == "Share"){
        showhat = (
          <div style={{height: '100%'}}>
                <div className="row">
                  <div className="col-sm-6">
                    <Paper style={exportstyle} zDepth={1}>
                      <h4>Update admin share</h4>
                          <br/>
                          <TextField style={{'font-size': '20px', width: '51%'}} floatingLabelText="Search coworking space" underlineShow={true} value={this.state.srchadrsshare} hintStyle={styles.hintText} onChange={this.srchChangeAdrsShare} onKeyUp={this.handleKeyDownSrchAdrsShare} underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}/>
                          <Paper align="left" id="dropdownShare" zDepth={1} style={{width: '50.5%', 'max-width': '50.5% !important','margin': '0 -10px 0 -10px'}} className="dropdownsrch">
                            <List>
                            {this.state.admincpnyshare.map((company) => (
                                  <ListItem primaryText={company.name} onClick={() => this.hideDropdownShare(company.id, company.name)}/>
                                ))}
                            </List>
                          </Paper>
                          <br/>
                          <TextField type="number" floatingLabelText="Share Pecentage" value={this.state.sharePerc} onChange={this.handleSharePerc} underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}/>
                          <br/>
                          <br/>
                          <RaisedButton label="Update" primary={true} onClick={this.updateShare}/>
 
                    </Paper>
                  </div>
                </div>
          </div>);
      }
      else if(this.state.show == "Security"){
        showhat = (
          <div style={{height: '100%'}}>
                <div className="row">
                  <div className="col-sm-6">
                    <Paper style={exportstyle} zDepth={1}>
                      <h4>Update password</h4>
                      <br/>
                      <TextField type="password" floatingLabelText="Current Password" value={this.state.currpass1} onChange={this.handleCurrentPass1} underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}/>
                      <br/>
                      <TextField type="password" floatingLabelText="New password" value={this.state.newpasswd} onChange={this.handleNewPasswd} underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}/>
                      <br/>
                      <TextField type="password" floatingLabelText="Confirm password" value={this.state.cfrmpasswd} onChange={this.handleCfrmPasswd} underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}/>
                      <br/>
                      <br/>
                      <RaisedButton label="Update" primary={true} onClick={this.handleUpdatePassword}/>
                    </Paper>
                  </div>
                  <div className="col-sm-6">
                    <Paper style={exportstyle} zDepth={1}>
                      <h4>Update username</h4>
                          <br/>
                          <TextField type="password" floatingLabelText="Current Password" value={this.state.currpass2} onChange={this.handleCurrentPass2} underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}/>
                          <br/>
                          <TextField type="text" floatingLabelText="New username" value={this.state.newusername} onChange={this.handleNewUsername} underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}/>
                          <br/>
                          <br/>
                          <RaisedButton label="Update" primary={true} onClick={this.handleUpdateUsername}/>
 
                    </Paper>
                  </div>
                </div>
          </div>);
      }

      const actionsbill = [
        <FlatButton
          label="Cancel"
          primary={true}
          onClick={()=>this.setState({openbilling: false})}
          disabled={this.state.disabledBillBtn}
        />,
        <FlatButton
          label="OK"
          primary={true}
          onClick={this.handleBilled}
          disabled={this.state.disabledBillBtn}
        />,
      ];
    
      const actionssend = [
        <FlatButton
          label="Cancel"
          primary={true}
          onClick={()=>this.setState({opengift: false, srchcustsend: ''})}
          disabled={this.state.disabledSend}
        />,
        <FlatButton
          label="OK"
          primary={true}
          onClick={this.handleSendPointsGift}
          disabled={this.state.disabledSend}
        />,
      ];

    const actions = [
      <FlatButton
        label="OK"
        primary={true}
        onClick={this.handleCloseAddAdmin}
      />,
    ];

    const actions2 = [
      <FlatButton
        disabled = {this.state.progstat}
        label="Cancel"
        primary={true}
        onClick={this.handleCloseAddItem}
      />,
      <FlatButton
        disabled = {this.state.progstat}
        label="OK"
        primary={true}
        onClick={this.handleInsertItem}
      />
    ]

    const AdminLogo = (<div className="divbig" onClick={this.handleClickPopup}>
              <Avatar size={50} className="avatarmenubig">{(this.state.info.length != 0)?this.state.info[0].name.substring(0,1): ""}</Avatar>
              <span className="avatartxtbig">{(this.state.info.length != 0)?this.state.info[0].name: ""}</span>
              <span className="avatarsubtxtbig">{(this.state.info.length != 0)?this.state.info[0].type: ""}</span>
              <Moreexpand className="expandicon" color="white"/>
              <Popover
                open={this.state.openpopup}
                anchorEl={this.state.anchorEl}
                anchorOrigin={{horizontal: 'middle', vertical: 'bottom'}}
                targetOrigin={{horizontal: 'middle', vertical: 'top'}}
                onRequestClose={this.handleRequestClosePopup}
                animation={PopoverAnimationVertical}
              >
                <Menu>
                  <MenuItem primaryText="Sign out" onClick={this.handleSignout}/>
                </Menu>
              </Popover>
             </div>);

    const Logged = (
              <IconMenu
                iconButtonElement={
                  <IconButton><MoreVertIcon color="white"/></IconButton>
                }
                targetOrigin={{horizontal: 'right', vertical: 'top'}}
                anchorOrigin={{horizontal: 'right', vertical: 'top'}}
              >
                <div className="menuavatar">
                  <Avatar size={55} className="avatarmenu">{(this.state.info.length != 0)?this.state.info[0].name.substring(0,1): ""}</Avatar>
                  <span className="avatartxt">{(this.state.info.length != 0)?this.state.info[0].name: ""}</span>
                  <span className="avatarsubtxt">{(this.state.info.length != 0)?this.state.info[0].type: ""}</span>
                </div>
                <MenuItem primaryText="Sign out" onClick={this.handleSignout}/>
              </IconMenu>
            );
    if(!this.state.session){
      return (<div className="loadParent"><div className="loadChild"><CircularProgress size={this.state.loadersize} thickness={this.state.loaderthick}/></div></div>);
    }
    return(
    <div>

        <AppBar
          className="appshad"
          style={{position: 'fixed'}}
          zDepth={this.state.shadow} 
          title={<div id="titleid" className="titleStyle"><span className="titleFont">{this.state.title}</span></div>}
          onLeftIconButtonClick	= {this.handleToggleDim}
          iconElementLeft={<IconButton><NavigationMenu/></IconButton>}
          iconElementRight={this.state.hideAvater ? Logged : AdminLogo}
        />

        <div className="contentContainer">     
          <div className="childcon">      
               {showhat}
            </div>
        </div>

        <Drawer open={this.state.opendrawer}>
          <div className="divdrawer">
            <Avatar src={require('./images/Logo.png')} size={40} className="logoavatars" backgroundColor="white" />
            <span className="cotxt" style={{fontSize: '25px'}}>THE COCIRCLE</span>
          </div>
          <MenuItem onClick={this.handleManageAdmins} leftIcon={<AdminMan />}>Manage Admins</MenuItem>
          <MenuItem onClick={this.handleBilling} leftIcon={<Payment />}>Transactions</MenuItem>
          <MenuItem onClick={this.handleRedemptions} leftIcon={<Redemption/>}>Redemptions</MenuItem>
          <MenuItem onClick={this.handleCustomers} leftIcon={<Account/>}>Customers</MenuItem>
          <MenuItem onClick={this.handleItems} leftIcon={<BillIcon />}>Billing Informations</MenuItem>
          <MenuItem onClick={this.handleSendPoints} leftIcon={<IInput />}>Gift Points</MenuItem>
          <MenuItem onClick={this.handleExport} leftIcon={<ExportTable />}>Export</MenuItem>
          <MenuItem onClick={this.handleUpdateShare} leftIcon={<UpdateShare />}>Update Share</MenuItem>
          <MenuItem onClick={this.handleSecurity} leftIcon={<SecurityIcon />}>Security</MenuItem>
        </Drawer>
        <Drawer
          docked={false}
          open={this.state.opendrawerdim}
          onRequestChange={(opendrawerdim) => this.setState({opendrawerdim})}
        >
          <div className="divdrawer">
            <Avatar src={require('./images/Logo.png')} size={40} className="logoavatars" backgroundColor="white" />
            <span className="cotxt" style={{fontSize: '25px'}}>THE COCIRCLE</span>
          </div>
          <MenuItem onClick={this.handleManageAdmins} leftIcon={<AdminMan />}>Manage Admins</MenuItem>
          <MenuItem onClick={this.handleBilling} leftIcon={<Payment />}>Transactions</MenuItem>
          <MenuItem onClick={this.handleRedemptions} leftIcon={<Redemption/>}>Redemptions</MenuItem>
          <MenuItem onClick={this.handleCustomers} leftIcon={<Account/>}>Customers</MenuItem>
          <MenuItem onClick={this.handleItems} leftIcon={<BillIcon />}>Billing Informations</MenuItem>
          <MenuItem onClick={this.handleSendPoints} leftIcon={<IInput />}>Gift Points</MenuItem>
          <MenuItem onClick={this.handleExport} leftIcon={<IInput />}>Export</MenuItem>
          <MenuItem onClick={this.handleUpdateShare} leftIcon={<UpdateShare />}>Update Share</MenuItem>
          <MenuItem onClick={this.handleSecurity} leftIcon={<SecurityIcon />}>Security</MenuItem>
        </Drawer>

      

        <Dialog
          title="Accesscode:"
          actions={actions}
          modal={true}
          contentStyle={customContentStyle}
          open={this.state.openaddadmin}>
          <div>
           <div className="container">
           <div className="row">
            <div className="col">
            <TextField type="number" fullWidth={true} floatingLabelFixed="true" floatingLabelText="Share percentage to co-working operator" value={this.state.share} onChange={this.handleShare} underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}/>
            <SelectField
              hidden
              fullWidth={true}
              floatingLabelText="Currency"
              value={this.state.currency}
              onChange={this.handleCurrency}
              underlineStyle={styles.underlineStyle} 
              floatingLabelStyle={styles.labelStyle} 
              floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
            >
              <MenuItem value="AED" primaryText="AED" />
              <MenuItem value="AFN" primaryText="AFN" />
              <MenuItem value="ALL" primaryText="ALL" />
              <MenuItem value="AMD" primaryText="AMD" />
              <MenuItem value="ANG" primaryText="ANG" />
              <MenuItem value="AOA" primaryText="AOA" />
              <MenuItem value="ARS" primaryText="ARS" />
              <MenuItem value="AUD" primaryText="AUD" />
              <MenuItem value="AWG" primaryText="AWG" />
              <MenuItem value="AZN" primaryText="AZN" />
              <MenuItem value="BAM" primaryText="BAM" />
              <MenuItem value="BBD" primaryText="BBD" />
              <MenuItem value="BDT" primaryText="BDT" />
              <MenuItem value="BGN" primaryText="BGN" />
              <MenuItem value="BHD" primaryText="BHD" />
              <MenuItem value="BIF" primaryText="BIF" />
              <MenuItem value="BMD" primaryText="BMD" />
              <MenuItem value="BND" primaryText="BND" />
              <MenuItem value="BOB" primaryText="BOB" />
              <MenuItem value="BRL" primaryText="BRL" />
              <MenuItem value="BSD" primaryText="BSD" />
              <MenuItem value="BTC" primaryText="BTC" />
              <MenuItem value="BTN" primaryText="BTN" />
              <MenuItem value="BWP" primaryText="BWP" />
              <MenuItem value="BYN" primaryText="BYN" />
              <MenuItem value="BZD" primaryText="BZD" />
              <MenuItem value="CAD" primaryText="CAD" />
              <MenuItem value="CDF" primaryText="CDF" />
              <MenuItem value="CHF" primaryText="CHF" />
              <MenuItem value="CLF" primaryText="CLF" />
              <MenuItem value="CLP" primaryText="CLP" />
              <MenuItem value="CNH" primaryText="CNH" />
              <MenuItem value="CNY" primaryText="CNY" />
              <MenuItem value="COP" primaryText="COP" />
              <MenuItem value="CRC" primaryText="CRC" />
              <MenuItem value="CUC" primaryText="CUC" />
              <MenuItem value="CUP" primaryText="CUP" />
              <MenuItem value="CVE" primaryText="CVE" />
              <MenuItem value="CZK" primaryText="CZK" />
              <MenuItem value="DJF" primaryText="DJF" />
              <MenuItem value="DKK" primaryText="DKK" />
              <MenuItem value="DOP" primaryText="DOP" />
              <MenuItem value="DZD" primaryText="DZD" />
              <MenuItem value="EGP" primaryText="EGP" />
              <MenuItem value="ERN" primaryText="ERN" />
              <MenuItem value="ETB" primaryText="ETB" />
              <MenuItem value="EUR" primaryText="EUR" />
              <MenuItem value="FJD" primaryText="FJD" />
              <MenuItem value="FKP" primaryText="FKP" />
              <MenuItem value="GBP" primaryText="GBP" />
              <MenuItem value="GEL" primaryText="GEL" />
              <MenuItem value="GGP" primaryText="GGP" />
              <MenuItem value="GHS" primaryText="GHS" />
              <MenuItem value="GIP" primaryText="GIP" />
              <MenuItem value="GMD" primaryText="GMD" />
              <MenuItem value="GNF" primaryText="GNF" />
              <MenuItem value="GTQ" primaryText="GTQ" />
              <MenuItem value="GYD" primaryText="GYD" />
              <MenuItem value="HKD" primaryText="HKD" />
              <MenuItem value="HNL" primaryText="HNL" />
              <MenuItem value="HRK" primaryText="HRK" />
              <MenuItem value="HTG" primaryText="HTG" />
              <MenuItem value="HUF" primaryText="HUF" />
              <MenuItem value="IDR" primaryText="IDR" />
              <MenuItem value="ILS" primaryText="ILS" />
              <MenuItem value="IMP" primaryText="IMP" />
              <MenuItem value="INR" primaryText="INR" />
              <MenuItem value="IQD" primaryText="IQD" />
              <MenuItem value="IRR" primaryText="IRR" />
              <MenuItem value="ISK" primaryText="ISK" />
              <MenuItem value="JEP" primaryText="JEP" />
              <MenuItem value="JMD" primaryText="JMD" />
              <MenuItem value="JOD" primaryText="JOD" />
              <MenuItem value="JPY" primaryText="JPY" />
              <MenuItem value="KES" primaryText="KES" />
              <MenuItem value="KGS" primaryText="KGS" />
              <MenuItem value="KHR" primaryText="KHR" />
              <MenuItem value="KMF" primaryText="KMF" />
              <MenuItem value="KPW" primaryText="KPW" />
              <MenuItem value="KRW" primaryText="KRW" />
              <MenuItem value="KWD" primaryText="KWD" />
              <MenuItem value="KYD" primaryText="KYD" />
              <MenuItem value="KZT" primaryText="KZT" />
              <MenuItem value="LAK" primaryText="LAK" />
              <MenuItem value="LBP" primaryText="LBP" />
              <MenuItem value="LKR" primaryText="LKR" />
              <MenuItem value="LRD" primaryText="LRD" />
              <MenuItem value="LSL" primaryText="LSL" />
              <MenuItem value="LYD" primaryText="LYD" />
              <MenuItem value="MAD" primaryText="MAD" />
              <MenuItem value="MDL" primaryText="MDL" />
              <MenuItem value="MGA" primaryText="MGA" />
              <MenuItem value="MKD" primaryText="MKD" />
              <MenuItem value="MMK" primaryText="MMK" />
              <MenuItem value="MNT" primaryText="MNT" />
              <MenuItem value="MOP" primaryText="MOP" />
              <MenuItem value="MRO" primaryText="MRO" />
              <MenuItem value="MRU" primaryText="MRU" />
              <MenuItem value="MUR" primaryText="MUR" />
              <MenuItem value="MVR" primaryText="MVR" />
              <MenuItem value="MWK" primaryText="MWK" />
              <MenuItem value="MXN" primaryText="MXN" />
              <MenuItem value="MYR" primaryText="MYR" />
              <MenuItem value="MZN" primaryText="MZN" />
              <MenuItem value="NAD" primaryText="NAD" />
              <MenuItem value="NGN" primaryText="NGN" />
              <MenuItem value="NIO" primaryText="NIO" />
              <MenuItem value="NOK" primaryText="NOK" />
              <MenuItem value="NPR" primaryText="NPR" />
              <MenuItem value="NZD" primaryText="NZD" />
              <MenuItem value="OMR" primaryText="OMR" />
              <MenuItem value="PAB" primaryText="PAB" />
              <MenuItem value="PEN" primaryText="PEN" />
              <MenuItem value="PGK" primaryText="PGK" />
              <MenuItem value="PHP" primaryText="PHP" />
              <MenuItem value="PKR" primaryText="PKR" />
              <MenuItem value="PLN" primaryText="PLN" />
              <MenuItem value="PYG" primaryText="PYG" />
              <MenuItem value="QAR" primaryText="QAR" />
              <MenuItem value="RON" primaryText="RON" />
              <MenuItem value="RSD" primaryText="RSD" />
              <MenuItem value="RUB" primaryText="RUB" />
              <MenuItem value="RWF" primaryText="RWF" />
              <MenuItem value="SAR" primaryText="SAR" />
              <MenuItem value="SBD" primaryText="SBD" />
              <MenuItem value="SCR" primaryText="SCR" />
              <MenuItem value="SDG" primaryText="SDG" />
              <MenuItem value="SEK" primaryText="SEK" />
              <MenuItem value="SGD" primaryText="SGD" />
              <MenuItem value="SHP" primaryText="SHP" />
              <MenuItem value="SLL" primaryText="SLL" />
              <MenuItem value="SOS" primaryText="SOS" />
              <MenuItem value="SRD" primaryText="SRD" />
              <MenuItem value="SSP" primaryText="SSP" />
              <MenuItem value="STD" primaryText="STD" />
              <MenuItem value="STN" primaryText="STN" />
              <MenuItem value="SVC" primaryText="SVC" />
              <MenuItem value="SYP" primaryText="SYP" />
              <MenuItem value="SZL" primaryText="SZL" />
              <MenuItem value="THB" primaryText="THB" />
              <MenuItem value="TJS" primaryText="TJS" />
              <MenuItem value="TMT" primaryText="TMT" />
              <MenuItem value="TND" primaryText="TND" />
              <MenuItem value="TOP" primaryText="TOP" />
              <MenuItem value="TRY" primaryText="TRY" />
              <MenuItem value="TTD" primaryText="TTD" />
              <MenuItem value="TWD" primaryText="TWD" />
              <MenuItem value="TZS" primaryText="TZS" />
              <MenuItem value="UAH" primaryText="UAH" />
              <MenuItem value="UGX" primaryText="UGX" />
              <MenuItem value="USD" primaryText="USD" />
              <MenuItem value="UYU" primaryText="UYU" />
              <MenuItem value="UZS" primaryText="UZS" />
              <MenuItem value="VEF" primaryText="VEF" />
              <MenuItem value="VES" primaryText="VES" />
              <MenuItem value="VND" primaryText="VND" />
              <MenuItem value="VUV" primaryText="VUV" />
              <MenuItem value="WST" primaryText="WST" />
              <MenuItem value="XAF" primaryText="XAF" />
              <MenuItem value="XAG" primaryText="XAG" />
              <MenuItem value="XAU" primaryText="XAU" />
              <MenuItem value="XCD" primaryText="XCD" />
              <MenuItem value="XDR" primaryText="XDR" />
              <MenuItem value="XOF" primaryText="XOF" />
              <MenuItem value="XPD" primaryText="XPD" />
              <MenuItem value="XPF" primaryText="XPF" />
              <MenuItem value="XPT" primaryText="XPT" />
              <MenuItem value="YER" primaryText="YER" />
              <MenuItem value="ZAR" primaryText="ZAR" />
              <MenuItem value="ZMW" primaryText="ZMW" />
              <MenuItem value="ZWL" primaryText="ZWL" />
            </SelectField>
            <br/>
            <br/>
            <RaisedButton onClick={this.handleCreateCode} fullWidth={true} label="Generate" primary={true}/>
            </div>
            </div>
           </div>
           </div>
           <br/>
           <br/>
           <div align="center" style={{'font-weight': '700', 'background-color': '#fdfdfd', 'padding': '10px', 'border': '0.5px solid #e0e0e0','border-radius': '2px'}}>
            <h1>{this.state.acscode}</h1>
           </div>
           
        </Dialog>

        <Dialog
          autoScrollBodyContent	= {true}
          autoDetectWindowHeight = {true}
          title="Add Item"
          actions={actions2}
          modal={true}
          contentStyle={customContentStyle2}
          open={this.state.openadditem}>
          {this.state.progstat?<LinearProgress mode="indeterminate" style={{'position':'absolute', 'margin-top':'-76px', 'margin-left':'-24px'}}/>:<div></div>}
          <br/>
          <div className="previewComponent">
            <div className="row">
              <div className="col-sm-6">
              <div>
                {$imagePreview}
              </div>
              </div>
              
              <div className="col-sm-6">
                <div className="row">                
                <div className="col">
                <RaisedButton primary={true} label="Choose file" onClick={this._openFileDialog} disabled = {this.state.progstat}/>
                </div>
                <div className="col">
                <p id="picname" style={{'margin-top':'7px'}}></p>
                </div>
                </div>
                <input 
                ref={input => this.inputElement = input}
                className="fileInput" 
                type="file" 
                onChange={(e)=>this._handleImageChange(e)} 
                style={{"display" : "none"}}
                />

                <TextField
                  disabled = {this.state.progstat}
                  floatingLabelText="Name"
                  value={this.state.itemname}
                  onChange={this.handleChangeItemName}
                  fullWidth="true" hintStyle={styles.hintText} underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                />
                <TextField
                  disabled = {this.state.progstat}
                  floatingLabelText="Price"
                  value={this.state.itemprice}
                  onChange={this.handleChangeItemPrice}
                  fullWidth="true" hintStyle={styles.hintText} underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                />
                <TextField
                  disabled = {this.state.progstat}
                  floatingLabelText="Description"
                  value={this.state.itemdesc}
                  onChange={this.handleChangeItemDesc}
                  fullWidth="true" hintStyle={styles.hintText} underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                />

              </div>
              </div>
          </div>
        </Dialog>

        <Dialog
          style={{overflow: 'hidden'}}
          title="Gift Points"
          actions={actionssend}
          modal={true}
          contentStyle={customContentStyle} 
          open={this.state.opengift}>
          
          <div className="container">
            <div className="row">
            <div style={{width: '30px'}}>
            <SearchIcon style={{color: '#757575', width: 30, height: 30, 'margin-top': '35px'}}/>
            </div>
            <div className="col" style={{'padding-right': '7px', 'padding-left': '10px'}}>
            <TextField
              fullWidth="true" 
              hintStyle={styles.hintText} 
              underlineStyle={styles.underlineStyle} 
              floatingLabelStyle={styles.labelStyle} 
              floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
              value={this.state.srchcustsend}
              onChange={this.handleSearchCustomerChangeSend}
              onKeyUp={this.handleKeyDownSrchSend}
              floatingLabelText="Search member"
              type="text"
              disabled={this.state.disabledSend}
            />
            </div>
            </div>
          </div>
          <Paper id="dropdowngift" zDepth={1} style={{width: this.state.paperdivsend}} className="paperSrch">
             <List>
               {this.state.customerssend.map((customers) => (
                  <ListItem primaryText={customers.name} onClick={() => this.hideDropdownSend(customers.id, customers.name, customers.points)}/>
                ))}
            </List>
          </Paper>

            <div className="container">
            <div className="row">
            <div style={{width: '30px'}}>
            <Payment style={{color: '#757575', width: 30, height: 30, 'margin-top': '35px'}}/>
            </div>
            <div className="col" style={{'padding-right': '7px', 'padding-left': '10px'}}>
            <TextField
              fullWidth="true" 
              hintStyle={styles.hintText} 
              underlineStyle={styles.underlineStyle} 
              floatingLabelStyle={styles.labelStyle} 
              floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
              value={this.state.ptssend} 
              onChange={this.handlePointstoSend}
              floatingLabelText="Points"
              type="number"
              disabled={this.state.disabledSend}
            />
            </div>
            </div>
            </div>
          
        </Dialog>

        <Dialog
          title="Warning"
          actions={actionsbill}
          modal={true}
          open={this.state.openbilling}
        >
          Are you sure this is settled?
        </Dialog>
    </div>);
}
  
}

