import React , {Component} from 'react';
import FontIcon from 'material-ui/FontIcon';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import Avatar from 'material-ui/Avatar';
import Checkbox from 'material-ui/Checkbox'
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import DatePicker from 'material-ui/DatePicker';
import './AccessCode.css';
import {blue100, blue500, blue700} from 'material-ui/styles/colors';
import Login from './Login';
import axios from 'axios';
import Fin from 'mui-icons/evilicons/check';
import Connecton from './Connection'

const server = Connecton.server;
const serverphp = Connecton.serverphp;
const protocol = Connecton.protocol;

// console.log(Connecton.server)

// const server = 'localhost:3000';
// const serverphp = 'localhost:80';


const styles = {
  underlineStyle: {
    borderColor: '#9e9e9e',
  },
  labelStyle: {
    color: '#9e9e9e',
  },
  floatingLabelFocusStyle: {
    color: blue500,
  },
};

export default class AccessCode extends Component {

constructor(props) {
  super(props);
  this.state = {
    email: null,
  };
  this.handleSendMail = this.handleSendMail.bind(this);
}

componentDidMount() {

}

handleEmail = (event) => {
    this.setState({
      email: event.target.value,
    });
};



handleSendMail(event) {
  axios.get(protocol+'://'+serverphp+'/php/sendEmail.php?email='+this.state.email)
    .then(res => {
      if(res.data.trim() == "Mail Sent!"){
        alert(res.data)
        window.location.replace(protocol+"://"+server+'/Login')
      }else {
          alert(res.data);
      }
  });
}


render(){

  return (
      <div>
        <div className="body">
        <div className="back">
          
        </div>
        </div>
        <div className="contmainp">
          <div className="contchildp" >
            <Avatar src={require('./images/Logo.png')} size={100} className="logos" backgroundColor="white"/>
            <div className="childcontentp">
              <br/>
              <br/>
              <br/>
              <br/>
              <div>
                  <div className="blockdivsp">
                    <h4 className="bluetxt" style={{color: blue500}}>FORGOT PASSWORD</h4>
                    <TextField  value={this.state.email} onChange={this.handleEmail} floatingLabelText="Enter email" fullWidth="true" underlineStyle={styles.underlineStyle} floatingLabelStyle={styles.labelStyle} floatingLabelFocusStyle={styles.floatingLabelFocusStyle}/>
                  </div>
                  <div >
                    <RaisedButton label="SUBMIT" primary={true} className="btncenter" onClick={this.handleSendMail}/>
                  </div> 
                  <br/>
                  <div align="center">
                    <a className="havacc" onClick={() => window.location.replace(protocol+"://"+server+'/Login')}>Login</a>
                    <br/>
                    <br/>
                    <br/>
                  </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    );


}

}

