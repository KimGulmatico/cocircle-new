import React from 'react';
import { Switch, Route } from 'react-router-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {blue100, blue500, blue700} from 'material-ui/styles/colors';
import Login from './Login';
import Register from './Register';
import SuperAdminAcc from './SuperAdminAcc';
import AdminAcc from './AdminAcc';
import AccessCode from './AccessCode';
import RegAdmin from './RegAdmin';
import Haveacc from './haveacc';
import User from './Customer';
import Manual from './ManualSignup';
import ForgotPassword from './Forgotpassword';
import Reset from './Reset';
import Ridirect from './Ridirect';
import './App.css';


const muiTheme = getMuiTheme({
  palette: {
    primary1Color: blue500,
    primary2Color: blue700,
    primary3Color: blue100,
  },
  menuItem: {
    selectedTextColor: blue500,
  },
  tabs: {
    backgroundColor: 'white',
  },
  inkBar: {
    backgroundColor: blue500
  }
  //userAgent: req.headers['user-agent'],
});

const App = () => (
  <MuiThemeProvider muiTheme={muiTheme}>
    <Switch>
    <Route path = "/Login" component={Login}/>
    <Route path = "/Register" component={Register}/>
    <Route path = "/SuperAdmin" component={SuperAdminAcc}/>
    <Route path = "/Admin" component={AdminAcc}/>
    <Route path = "/RegAdmin" component={RegAdmin}/>
    <Route path = "/Haveaccount" component={Haveacc}/>
    <Route path = "/AccessCode" component={AccessCode}/>
    <Route path = "/User" component={User}/>
    <Route path = "/Manual" component={Manual}/>
    <Route path = "/ForgotPassword" component={ForgotPassword}/>
    <Route path = "/Reset" component={Reset}/>
    <Route exact path = "/" component={Ridirect}/>
    </Switch>
  </MuiThemeProvider>
);

export default App;


 
